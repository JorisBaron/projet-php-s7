# Ouais!-b

## Introduction

***[Ouais!-b](https://ouais-b.jooj.io)*** est une application de type photothèque permettant de partager des oeuvres photographiques de toute sorte
et propose également une possibilité de connexion avec d'autres créateurs de contenus.
Premier concurrent d'*AIRstagram*, soyez les premiers à découvrir notre toute nouvelle plateforme numérique au style minimaliste unique.


## Utilisation

L'utilisation de ***Ouais!-b*** est simple :
vous pouvez consulter les profils de n'importe quel utilisateur sans avoir besoin de créer un compte tant que ces profils sont définis comme publiques.
Les recherches par pseudo et le choix du dark ou light theme sont également disponibles et, évidemment, l'inscription (gratuite et interdite aux moins de 13 ans) et la connexion en cas d'utilisateur existant.

Un utilisateur existant de ***Ouais!-b*** authentifié peut gérer ses photos, ses informations personnelles,
le caractère privé ou pas de sa page et regarder le profil de ses amis même déclaré privé.
Le nombre total de photos n'est pas restreint mais elles ne peuvent être affichées qu'au nombre de 9 sur une même page, dans un format 3x3.

## Fonctionnalités

### Gestion de profil

L'utilisateur dispose d'un nom, un prénom, un pseudo qu'il utilise comme nom d'utilisateur, d'une date de naissance, d'une adresse e-mail et d'un mot de passe.
Il est libre de les modifier dans la limite des conditions d'utilisation.

Son profil peut être visible publiquement ou non en fonction de ses préférences. Ce paramètre peut être modifiable également sur sa page de paramètres.

Sa photo de profil, son pseudo, son nom et son prénom sont affichés en haut de sa page de profil

### Gestion de photos

Une photo est définie par son titre, son fichier, sa description, son album et ses tags.
Elle ne peut être placée que dans un seul album ou dans aucun.

L'utilisateur peut publier des photos sur sa page qui apparaîtront par groupe de 9 en format 3x3.
S'il en a plus de 9, un outil de pagination sera alors visible en bas de document permettant de changer de page et visualiser ainsi le reste des photos.
Il n'y a pour le moment pas de limites quant au nombre de photos possiblement publiables.

La publication des photos est disponible uniquement sur la propre page d'un utilisateur.
Elle demande nécessairement un titre et un fichier, les autres champs étant optionnels.

Une fois publiée, on peut visualiser une photo en cliquant sur sa miniature sur la page de profil. Ses détails sont également visibles à ce moment.

Pour le moment, c'est uniquement lors de l'étape de visualisation d'une photo que l'on peut accéder aux fonctionnalités d'édition et de suppression.

L'édition permettra de modifier tous les champs sauf le champ de fichier.

La suppression requerra une confirmation.

Lorsqu'un utilisateur se trouve sur une page ne lui appartenant pas, il ne peut que visualiser les photos présentes.
C'est uniquement lorsqu'il clique sur l'une d'elles qu'il peut accéder aux fonctionnalités de signalement et de lien de partage de la photo en question.

Qu'un utilisateur soit connecté ou non, il est capable de trier par date, par tag, par titre et de filtrer par date, par tags les photos présentes.


### Gestion d'album

La gestion d'albums se fait dans l'onglet "album" de la page de profil.

Un album est une sorte de dossier propre à l'application. Il permet de regrouper des photos à un seul endroit.
Il possède un titre et une liste de photos.

Ainsi, lors de la visualisation d'une page de profil, on peut demander à n'afficher uniquement que les photos appartenant à un album en particulier.

On peut créer un album vide, modifier le titre, y ajouter une ou plusieurs photos, y supprimer une ou plusieurs photos et rechercher un album par son titre ou sa date de création.


### Gestion de photos supprimées (corbeille)

Les photos supprimées depuis la page de profil sont envoyées en réalité au préalable dans une corbeille.
Au bout de 30 jours après placement dans la corbeille, la photo placée est alors détruite définitivement.
Il reste possible à l'utilisateur propriétaire de cette photo de supprimer manuellement depuis la corbeille définitivement celle-ci,
voire d'en supprimer plusieurs à la fois.

### Gestion d'amis

Deux utilisateurs inscrits peuvent être amis après que l'un d'eux ait fait une demande en ami que l'autre a accepté.
L'utilité de cette fonctionnalité permet aux utilisateurs amis de voir le profil de son ami, qu'il soit public ou privé.

À l'avenir, cette fonctionnalité pourra permettre de visualiser un fil d'actualité construit
à partir des photos publiées par ses amis, ordonnées de la plus récente à la plus ancienne.

### Gestion d'administration

Optionnellement, l'application disposera d'un module d'administration permettant à des administrateurs de gérer les signalements de photos.
Un signalement pourra alors soit être annulé, soit accepté et, de ce fait, supprimer la photo sensible.
Dans les deux cas, à l'issue de la gestion d'un signalement, le signalement est supprimé.


## Notes de l'équipe

Nous sommes les Kips. Nous développons surtout parce qu'on doit valider notre année mais aussi par plaisir de développer un projet riche et passionnant.
Nous adorons rire ! Aussi, avec un peu de patience, vous pourrez peut-être trouver des easters eggs dans notre application 😉

N'hésitez pas à nous faire des retours via notre adresse mail [ouais-b@jooj.io](mailto:ouais-b@jooj.io) !