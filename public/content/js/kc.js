/**
 * Easter egg
 * @author Joris Baron
 * @link https://fr.wikipedia.org/wiki/Code_Konami
 */

$(document).ready(function(){
	const ee1 = [38,38,40,40,37,39,37,39,66,65];
	let ee1c = 0;
	$('html').on('keydown', function(e){
		if(e.which === ee1[ee1c]){
			ee1c++;
			if(ee1c >= 10){
				$('body').addClass('dabr');
				setTimeout(function(){
					$('body').removeClass('dabr');
					ee1c=0;
				},2020)
			}
		}
	})
})