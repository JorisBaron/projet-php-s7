/**
 * @file album.js
 * définit toutes les fonctions js de la partie album
 *
 * Auteurs pour ce fichier (sauf contre-indications pour une fonction)
 * @author Maurane Glaude
 */

let body = $("body");

/**************************/
/* CHARGEMENT DES ALBUMS */
/*************************/

// chargement de la liste des albums
$("body").on("show.bs.tab", "#album-tab",function() {
    Spinner.show();
    const url = $(this).data("url");
    if(url) {
        $.get({
            url: url,
            complete:function(){
                Spinner.hide();
            },
            success: function(html) {
                $("#album-container").html(html);
            }
        });
    }
});

// tri des éléments
$("body").on("click", "#sort-albums-btn",function(e) {
    e.preventDefault();

    Spinner.show();

    const url = $(this).attr("href");

    if(url) {
        $.get({
            url: url,
            success: function(html) {
                $("#album-container").html(html);
                Spinner.hide();
            }
        });
    }
});

// pagination
$("body").on("click", "#album-row #album-pagination a.page-link", function(e) {
    e.preventDefault();

    Spinner.show();

    const url = Routing.generate('album.grid.get', {
        pseudo: profilPseudo,
        page: $(this).data("page"),
        order: $("#sort-albums-btn").data('order'),
    });

    let albumContainer = $("#album-container");

    $.get({
        url: url,
        success: function(html) {
            albumContainer.html(html);
            Spinner.hide();
        }
    })
})

/************************************/
/* GESTION - PAGE DE LISTE D'ALBUMS */
/************************************/

// affichage d'un album
$("body").on("click", "#album-row .thumbnail", function(e) {
    Spinner.show();
    e.preventDefault();
    $.get({
        url: $(this).attr("href"),
        complete: function(){
            Spinner.hide();
        },
        success: function (html) {
            $("#album-container").html(html);
        }
    });
});

/*********************************************/
/* GESTION - PAGE LISTE DE PHOTOS D'UN ALBUM */
/*********************************************/

// pagination
$("body").on("click", "#album-photo-pagination #album-pagination a.page-link", function(e) {
    e.preventDefault();

    Spinner.show();

    const url = Routing.generate('album.get', {
        pseudo: profilPseudo,
        page: $(this).data("page"),
    });

    let albumContainer = $("#album-container");

    $.get({
        url: url,
        success: function(html) {
            albumContainer.html(html);
            Spinner.hide();
        }
    })
})

// quitter un album
/* permet de revenir à la page de liste des albums */
$("body").on("click", "#exit", function(e) {
    e.preventDefault();
    Spinner.show();

    $.get({
        url: $(this).attr("href"),
        complete: function(){
            Spinner.hide();
        },
        success: function(html) {
            $("#album-container").html(html);
        }
    });

});

// affichage d'une photo dans un album
$("body").on("click", ".album-photo-container", function (e) {
    e.preventDefault();

    getAlbumPhotoDetail($(this).data('url'));
});

// fonction permettant d'obtenir une photo
function getAlbumPhotoDetail(url) {
    Spinner.show();
    $.get({
        url: url,
        complete: function(){
            Spinner.hide();
        },
        success: function (html) {
            $("#AlbumPhotoDetailContainer").html(html);
        }
    });
}

// partage de lien
// équivalent à la fonction de partage de 'photo.js'
// sans gestion d'historique
$("body").on('click', '#modalShowAlbumPhoto #sharePhoto', function() {
    navigator
        .clipboard.writeText(Routing.generate('profile.display.photo', {
            pseudo: profilPseudo,
            photo: $("#photoDetailPhoto").data('photo-id')
        }, true))
        .then(
            r => {
                displayAlbumAlertCopied("Lien copié avec succès.",'Merci !');
            },
            r => {
                displayAlbumAlertCopied("Le lien n'as pas pu être copié.",'Ah dommage !');
        });
    });

// promise du partage de lien
function displayAlbumAlertCopied(message, btnText){
    let modalPhoto = $('#modalShowAlbumPhoto');

    modalPhoto.modal('hide');

    new GenericAlert({
        message : message,
        buttonText : btnText,
        onHide : () => {
            modalPhoto.modal('show');
        }
    });
}