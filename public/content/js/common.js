/**
 * JS commun à toutes les pages du site
 * @author Joris Baron
 */


$(document).ready(function() {
	// initialise les <input type="file"> de Bootstrap pour pouvoir afficher le nom de fichier
	bsCustomFileInput.init()
});

/**
 * Remplace l'HTML de "destElement" présent dans le DOM avec l'HTML de "srcElement" présent dans "html"
 * Utile notamment pour le chargement d'HTML via AJAX
 * @param html
 * @param srcElement
 * @param destElement
 */
function loadHtml(html, srcElement, destElement = srcElement){
	$(destElement).html(
		$('<div>').append(html).find(srcElement).html()
	);
}