/**
 * @file owner-page.js
 *
 * Auteur pour ce fichier (sauf contre-indications pour une fonction)
 * @author Joris Baron
 * @author Maurane Glaude
 * @author Tinhinane Kadri
 */

$(function(){
	bindAllOwnerPageEvent();
})

/**
 * @author Joris Baron
 */
$(window).on('popstate', function(event) {
	event.preventDefault();
	bindAllOwnerPageEvent();
});

function bindAllOwnerPageEvent(){
	bindAddForm();
	bindDisplayPhotoEditForm();
	bindPhotoCorbeille();
}

function bindAddForm(){
	const modalAddPhoto = $("#modalAddPhotoForm")

	bindFormTagsEvents(modalAddPhoto);
	bindPhotoAddSubmit();
}

function bindEditForm(photoId) {
	const modalEditPhoto = $("#modalEditPhotoForm");
	const editForm = modalEditPhoto.find("form");

	bindFormTagsEvents(editForm);

	editForm.on('submit', function(e){
		e.preventDefault();
		let form_data = $(this).serialize();

		// const submitBtn = $(this).find('button[type="submit"]');

		Spinner.show();

		$.ajax({
			method: "PUT",
			url: Routing.generate('photo.edit', {
				photo_id: photoId
			}),
			data: form_data,
			complete: function(){
				Spinner.hide();
			},
			success: function() {
				Spinner.show();
				window.location.replace(
					Routing.generate('profile.display.photo', {pseudo: profilPseudo, photo: photoId}, true)
				);
				modalEditPhoto.modal('hide');
				// loadPhotoDetail(photoId);
				// loadPhotoGrid(profilPseudo, currentPage, true);
			},
			statusCode:{
				400: (jqxhr)=>{ // formulaire non valide
					const photoEditContainer = $('#photoEditContainer');
					photoEditContainer.html(jqxhr.responseText);
					bindEditForm(photoId)
				},
				401: ()=>{
					generic401Handler("Vous ne pouvez pas éditer de photo car vous n'êtes pas connecté");
				},
				403: ()=>{
					new GenericAlert({
						message: "Vous n'avez pas l'autorisation de modifier cette photo",
						buttonText: "Compris",
						onHide: ()=>{
							ModalPhotoDetailBinder.backOnHide.bind();
							modalPhotoDetail.modal('show');
						}
					});
				},
				404: ()=>{
					new GenericAlert({
						message : "La photo demandée n'existe pas",
						buttonText : 'Je vais recharger la page',
						onHide: () => {
							window.history.back();
							Spinner.show();
							window.location.replace(
								Routing.generate('profile', {pseudo: profilPseudo}, true)
							);
						}
					});
				}
			}
		});
	});
}

function bindFormTagsEvents(container){
	$(container).on('click', '.tag-btn > i[role="button"]', function(){
		$(this).closest('.tag-btn').remove();
	});
	$(container).find('.tag-value').on('keydown', function(e){
		if(e.which === 13) {
			e.preventDefault();
			addTag(container);
		}
	});
	$(container).find('.add-tag-btn').on('click', function(e){
		addTag(container);
	});
}

function bindPhotoAddSubmit(){
	$("#modalAddPhotoForm form").on('submit', function(event){
		event.preventDefault();

		Spinner.show()

		const thisForm = $(this);

		$.post({
			url: Routing.generate('photo.add'),

			data: new FormData(this),

			contentType:false,
			processData:false,
			cache:false,
			complete: function(){
				Spinner.hide();
			},
			success: function() {
				// reload profile if success
				thisForm[0].reset();
				Spinner.show();
				window.location.reload(false);
			},
			statusCode:{
				400: (jqxhr)=>{
					const photoAddFormContainer = $('#photoAddFormContainer');
					photoAddFormContainer.html(jqxhr.responseText);
					bindAddForm();
				},
				401: ()=>{
					generic401Handler("Vous ne pouvez pas ajouter de photo car vous n'êtes pas connecté");
				}
			}
		});
	});
}

function bindDisplayPhotoEditForm(){
	const modalPhotoDetail = $('#modalShowPhoto');
	const modalEditPhoto = $("#modalEditPhotoForm");

	modalPhotoDetail.on('click', '#editPhoto', function(){
		const photoEditContainer = $('#photoEditContainer');
		const photoId = $('#photoDetailPhoto').attr('data-photo-id');

		Spinner.show();

		$.get({
			url: Routing.generate('photo.edit.form', {
				photo_id: photoId
			}),
			complete: ()=>{
				Spinner.hide();
			},
			success: (resp)=>{
				photoEditContainer.html(resp);
				bindEditForm(photoId);

				ModalPhotoDetailBinder.backOnHide.unbind();
				modalEditPhoto.modal('show');
				modalPhotoDetail.modal('hide');
			},
			statusCode: {
				401: ()=>{
					ModalPhotoDetailBinder.backOnHide.unbind();
					modalPhotoDetail.modal('hide');
					generic401Handler("Vous ne pouvez pas modifier de photo car vous n'êtes pas connecté");
				},
				403: ()=>{
					ModalPhotoDetailBinder.backOnHide.unbind();
					modalPhotoDetail.modal('hide');
					new GenericAlert({
						message: "Vous n'avez pas l'autorisation de modifier cette photo",
						buttonText: "Compris",
						onHide: ()=>{
							ModalPhotoDetailBinder.backOnHide.bind();
							modalPhotoDetail.modal('show');
						}
					});
				},
				404: ()=>{
					ModalPhotoDetailBinder.backOnHide.unbind();
					modalPhotoDetail.modal('hide');
					new GenericAlert({
						message : "La photo demandée n'existe pas",
						buttonText : 'Je vais recharger la page',
						onHide: () => {
							window.history.back();
							Spinner.show();
							window.location.replace(
								Routing.generate('profile', {pseudo: profilPseudo}, true)
							);
						}
					});
				}
			}
		})
	});
}


function bindPhotoCorbeille(){
	const modalPhotoDetail = $('#modalShowPhoto');

	$('body').on('click', '#deletePhoto', function(){
		ModalPhotoDetailBinder.backOnHide.unbind();
		modalPhotoDetail.modal('hide');

		new GenericConfirmation({
			title: "Supprimer la photo",
			message:"Êtes-vous sûr de vouloir supprimer cette photo ?",

			buttonConfirmText:"Oui, je n'en veux plus !",
			buttonConfirmColorClass:'danger',
			onConfirm: throwPhotoAway,

			buttonCancelText:"Non, en fait je la veux garder",
			buttonCancelColorClass:'primary',
			onHide: () => {
				ModalPhotoDetailBinder.backOnHide.bind();
				modalPhotoDetail.modal('show');
			}
		});
	})


	function throwPhotoAway(){
		const photoId = $('#photoDetailPhoto').attr('data-photo-id');

		Spinner.show();

		$.ajax({
			method: "POST",
			url: Routing.generate('photo.throw-away', {photo_id: photoId}),
			complete: function(){
				Spinner.hide();
			},
			success: function(){
				new GenericAlert({
					message : 'Photo mise dans la corbeille avec succès !',
					buttonText : 'Merci !',
					onHide: () => {
						Spinner.show();
						window.location.replace(
							Routing.generate('profile', {pseudo: profilPseudo}, true)
						);
					}
				});
			},
			statusCode:{
				401: ()=>{
					generic401Handler("Vous ne pouvez pas mettre de photo dans la corbeille car vous n'êtes pas connecté");
				},
				403: ()=>{
					new GenericAlert({
						message : "Vous n'avez pas l'autorisation de mettre cette photo dans la corbeille",
						buttonText : 'Ah pardon je me suis trompé',
						onHide : ()=>{
							ModalPhotoDetailBinder.backOnHide.bind();
							modalPhotoDetail.modal('show');
						}
					});
				},
				404: ()=>{
					new GenericAlert({
						message : "La photo que vous essayer de mettre dans la corbeille n'existe pas",
						buttonText : 'Recharger la page',
						onHide: () => {
							window.history.back();
							Spinner.show();
							window.location.replace(
								Routing.generate('profile', {pseudo: profilPseudo}, true)
							);
						}
					});
				}
			}
		});
	}
}

/**
 * Handler générique pour erreur 401, invite l'utilisateur à se reconnecter
 *  en le redirigeant vers la page de login
 * @param message Message a afficher avant la redirection
 *
 * @param buttonText
 * @author Joris Baron
 */
function generic401Handler(message, buttonText){
	new GenericAlert(message, ()=>{
		Spinner.show();
		window.location.replace(
			Routing.generate('app_login', null, true)
		);
	}, undefined, buttonText || "Je vais aller me connecter dans ce cas");

}

/**
 * Ajoute la valeur du champ de saisie de tags à l'ensemble des tags de la photo
 * sur le formulaire passé en paramètre
 * @param form le formulaire à traiter
 */
function addTag(form) {
	const elt = $(form).find('.tag-value');
	const tag = elt.val().trim();

	if(tag !== "") {
		// retrieves the value of the new tag
		const tagGroup = $(form).find(".tag-group");

		// grab prototype data
		const prototype = tagGroup.data('prototype');
		// grab iteration
		const counter = tagGroup.data('widget-counter');

		// get element data (id and name)
		const tag_input_container = $(prototype.replace(/__name__/g, counter + '_text'));
		const tag_input = tag_input_container.children('input[type="hidden"]');
		tag_input.attr('value', tag);
		tag_input_container.children('span').text(tag);

		// if tag not an empty string
		if (tag.length > 0) {
			// append to tags container a new tag
			$(form).find(".tags-container").append(tag_input_container);
			tagGroup.data('widget-counter', counter+1);
		}

		// clear the tag input
		elt.val("");
	}
}

/**
 * @author Maurane Glaude
 */
$('body').on("click", "#editAlbumPhoto", function (e) {
	e.preventDefault();

	Spinner.show();

	let photoId = $("#photoDetailPhoto").data("photo-id");


	$.get({
		url: Routing.generate('photo.edit.album', {
			photo_id: photoId
		}),
		success: function(html) {
			$("#modalEditAlbumPhoto").modal('show');
			$("#modalEditAlbumPhotoContent").html(html);
			ModalPhotoDetailBinder.backOnHide.unbind();
			$("#modalShowPhoto").modal('hide');
			Spinner.hide();
		}
	});
});

/**
 * @author Maurane Glaude
 */
$('body').on("submit", "#photo_change_album_form", function(e){
	e.preventDefault();

	const formElm = $(this);
	const formData = $(this).serialize();
	let modalPhotoDetail = $("#modalShowPhoto");
	let photoDetailContainer = $("#photoDetailContainer");
	let modalEditAlbumPhoto = $("#modalEditAlbumPhoto");

	Spinner.show();

	$.post({
		url: formElm.data('url'),
		data: formData,
		success: function(url_success) {
			modalEditAlbumPhoto.modal('hide');

			modalEditAlbumPhoto.on("hidden.bs.modal", function() {
				$.get({
					url: url_success,
					success: function(html) {
						photoDetailContainer.html(html);
						Spinner.hide();
						ModalPhotoDetailBinder.backOnHide.bind();
						modalPhotoDetail.modal('show');
					}
				});
			});
	 	}
	});
});

/**
 * @author Maurane Glaude
 */
$('body').on("click", "#change_album_annuler", function(e) {
	e.preventDefault();
	let modalPhotoDetail = $("#modalShowPhoto");
	let modalEditAlbumPhoto = $("#modalEditAlbumPhoto");

	modalEditAlbumPhoto.modal('hide');
	ModalPhotoDetailBinder.backOnHide.bind();
	modalPhotoDetail.modal("show");
});


/**
 * FONCTIONS DE GESTIONS DES ALBUMS
 * @author Maurane Glaude
 */
/************************************/
/* GESTION - PAGE DE LISTE D'ALBUMS */
/************************************/

// création d'un nouvel album
$("body").on("click", "#add-album-btn", function (e) {
	e.preventDefault();

	Spinner.show();
	$.get({
		url: $(this).data("url"),
		complete: function(){
			Spinner.hide();
		},
		success: function(html) {
			$("#modalAddAlbumFormContent").html(html);
		}
	});
});

// édition d'un album
$("body").on("click", ".edit-album-btn", function(e) {
	e.preventDefault();
	Spinner.show();
	$.get({
		url: $(this).data("url"),
		complete: function(){
			Spinner.hide();
		},
		success: function(html) {
			$("#modalEditAlbumFormContent").html(html);
		}
	});
});

function post_form(id_form, id_modal) {
	let form = $(id_form);

	Spinner.show();
	// validation du formulaire
	$.post({
		url: form.data('url'),
		data: form.serialize(),
		success: function(url_success) {
			// cacher modal
			$(id_modal).modal('hide');

			// chargement de la page
			$.get({
				url: url_success,
				success: function (html) {
					$("#album-container").html(html);
					Spinner.hide();
				}
			});
		}
	});
}

// ajout d'un album
$("body").on("submit", "#album_add_form", function(e) {
	e.preventDefault();
	post_form("#album_add_form", "#modalAddAlbumForm");
});

// édition d'un album
$("body").on("submit", "#album_edit_form", function(e) {
	e.preventDefault();
	post_form("#album_edit_form", "#modalEditAlbumForm");
});

// suppression d'un album
$("body").on("click", ".delete-album-btn", function(e) {
	e.preventDefault();
	$("#album-delete-form").attr("action", $(this).data("url"));
});

// confirmation de suppression d'un album
$("body").on("submit", "#album-delete-form", function(e) {
	e.preventDefault();
	Spinner.show();

	$.ajax({
		method: "DELETE",
		url: $(this).attr("action"),
		success: function(url_success) {
			$("#modalDeleteAlbumForm").modal('hide');

			/* chargement de la page */
			$.get({
				url: url_success,
				success: function (html) {
					$("#album-container").html(html);
					Spinner.hide();
				}
			});
		}
	});
});

/*********************************************/
/* GESTION - PAGE LISTE DE PHOTOS D'UN ALBUM */
/*********************************************/

// édition d'une photo
$("body").on("click", "#AlbumPhotoDetailContainer #editPhoto", function(e) {
	e.preventDefault();

	let modalEditAlbumPhoto = $("#modalEditAlbumPhoto");
	let content = $("#modalEditAlbumPhotoContent");

	Spinner.show();

	$("#modalShowAlbumPhoto").modal('hide');

	$.get({
		url: Routing.generate('photo.edit.form', {photo_id: $("#photoDetailPhoto").data("photo-id")}),
		complete: ()=>{
			Spinner.hide();
		},
		success: function(html) {
			modalEditAlbumPhoto.modal('show');
			content.html(html);
			bindFormTagsEvents(modalEditAlbumPhoto);
		}
	});
});

// annulation de modification
$("body").on("click", "#modalEditAlbumPhotoContent #cancelDelete", function(e){
	e.preventDefault();

	let editModal = $("#modalEditAlbumPhoto");
	let showModal = $("#modalShowAlbumPhoto");
	editModal.modal('hide');

	editModal.on("hidden.bs.modal", function(){
		showModal.modal("show");
	});
})

// soumission du formulaire d'édition dans un album
$("body").on("submit", "#editPhotoForm", function(e) {
	e.preventDefault();
	let formElm = $(this);
	let formData = formElm.serialize();
	let photoId = formElm.data('photo-id');
	let editModal = $("#modalEditAlbumPhoto");

	Spinner.show();

	$.post({
		url: Routing.generate('photo.edit', {photo_id:photoId}),
		data: formData,
		complete: function() {
			Spinner.hide();
		},
		success: function() {
			editModal.modal('hide');

			editModal.on('hidden.bs.modal', function(){
				// relance la modal de détail de photo
				$("#modalShowAlbumPhoto").modal('show');
			});

			const url = Routing.generate('photo.get', {
				photo_id: photoId
			});

			getAlbumPhotoDetail(url);
		},

		error: function (jqxhr, err, status) {
			console.log(status);
		}
	})
});