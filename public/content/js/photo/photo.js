/**
 * @file photo.js
 * définit toutes les fonctions js de la partie publication de photos
 *
 * Auteurs pour ce fichier (sauf contre-indications pour une fonction)
 * @author Joris Baron
 * @author Maurane Glaude
 * @author Tinhinane Kadri
 */


/**
 * Gestion de l'utilisation de précédent/suivant de l'historique pour modifier l'URL à l'affichage d'une photo en grand
 * @author Joris Baron
 */
$(window).on('popstate', function(event) {
    event.preventDefault();

    const state = event.originalEvent.state;
    const stateBody = $('<div id="body">').html(state.body);

    $('#body main').html(stateBody.find('main').html());
    $('#modalsContainer').html(stateBody.find('#modalsContainer').html());
    $('#btn-notif-amis-container').html(stateBody.find('#btn-notif-amis-container').html())

    document.title = state.pageTitle;
    currentPage = state.profilePageNumber;

    if(!state.modal){
        $('.modal-backdrop').remove();
    } else {
        // $('body').append('<div class="modal-backdrop fade show"></div>');
        const modal = $(state.modal);
        if(modal.hasClass('fade')){
            modal.removeClass('fade').one('shown.bs.modal', function(){
                $(this).addClass('fade');
            });
        }
        modal.modal('show');
    }
});

// ON LOAD
$(function(e){
    pagination_handler();
    show_photo_handler();
    bindSharePhoto();
    bindReportPhoto();

    ModalPhotoDetailBinder.backOnHide.bind();

    window.history.replaceState({
        "body":$('#body').html(),
        "pageTitle":document.title
    }, "", Routing.generate('profile', {
        pseudo: profilPseudo,
        page : currentPage
    }));

    if(photoToLoad){
        // Spinner.show();
        loadPhotoDetail(photoToLoad)
    }
})


function pagination_handler() {
    $('body').on('click', '#photo-container .pagination-nav a.page-link', function(e){
        e.preventDefault();
        loadPhotoGrid(profilPseudo, $(this).attr('data-page'));
    });
}


function show_photo_handler() {
    $('body').on('click', '#photo-container .img-container a', function(e){
        e.preventDefault();
        loadPhotoDetail($(this).find('[data-photo-id]').attr('data-photo-id'))
    });
}

function bindSharePhoto() {
    $('body').on('click', '#modalShowPhoto #sharePhoto', function(){
        navigator
            .clipboard.writeText(window.location.href)
            .then(
                r => {
                    displayAlertCopied("Lien copié avec succès.",'Merci !');
                },
                r => {
                    displayAlertCopied("Le lien n'as pas pu être copié.",'Ah dommage !');
                });
    });

    function displayAlertCopied(message, btnText){
        const modalPhoto = $('#modalShowPhoto');

        ModalPhotoDetailBinder.backOnHide.unbind();
        modalPhoto.modal('hide');

        new GenericAlert({
            message : message,
            buttonText : btnText,
            onHide : () => {
                ModalPhotoDetailBinder.backOnHide.bind();
                modalPhoto.modal('show');
            }
        });
    }
}

function bindReportPhoto(){
    const modalPhoto = $('#modalShowPhoto');

    $('body').on('click', '#reportPhoto', function(){
        ModalPhotoDetailBinder.backOnHide.unbind();
        modalPhoto.modal('hide');

        new GenericConfirmation({
            title: "Signaler la photo",
            message:"Êtes-vous sûr de vouloir signaler cette photo ?",

            buttonConfirmText:"Oui, elle n'a rien à faire ici !",
            buttonConfirmColorClass:'danger',
            onConfirm: reportPhoto,

            buttonCancelText:"Non, en fait elle est cool",
            buttonCancelColorClass:'primary',
            onCancel: () => {
                ModalPhotoDetailBinder.backOnHide.bind();
                modalPhoto.modal('show');
            }
        });
    });

    function reportPhoto(){
        const photoId = $('#photoDetailPhoto').attr('data-photo-id');

        Spinner.show();
        $.post({
            url : Routing.generate('photo.report', {photo_id: photoId}),
            complete: function(){
                Spinner.hide();
            },
            success: function(){
                new GenericAlert({
                    message : 'Votre signalement a bien été pris en compte !',
                    buttonText : 'Merci !',
                    onHide: () => {
                        ModalPhotoDetailBinder.backOnHide.bind();
                        modalPhoto.modal('show');
                    }
                });
            },
            statusCode:{
                400: (jqxhr)=>{
                    let message, buttonText;
                    if(jqxhr.responseText === "alreadyReported"){
                        message = "Vous avez déjà signalé cette photo.";
                        buttonText = "Effectivement, désolé";
                    } else {
                        message = "Erreur lors du signalement de la photo.";
                    }
                    new GenericAlert({
                        message : message,
                        buttonText : buttonText,
                        onHide : ()=>{
                            ModalPhotoDetailBinder.backOnHide.bind();
                            modalPhoto.modal('show');
                        }
                    });
                },
                401: ()=>{
                    generic401Handler("Vous ne pouvez pas signalé cette photo car vous n'êtes pas connecté");
                },
                403: ()=>{
                    new GenericAlert({
                        message : "Vous n'avez pas l'autorisation de signaler cette photo",
                        buttonText : 'Ah pardon je me suis trompé',
                        onHide : ()=>{
                            ModalPhotoDetailBinder.backOnHide.bind();
                            modalPhoto.modal('show');
                        }
                    });
                },
                404: ()=>{
                    new GenericAlert({
                        message : "La photo que vous essayer de signaler n'existe pas",
                        buttonText : 'Recharger la page',
                        onHide: () => {
                            window.history.back();
                            Spinner.show();
                            window.location.replace(
                                Routing.generate('profile', {pseudo: profilPseudo}, true)
                            );
                        }
                    });
                }
            }
        });
    }
}

function loadPhotoGrid(pseudo, page=1, inBackground = false){
    const route = Routing.generate('profile', {
            pseudo:pseudo,
            page:page
        }, true);

    Spinner.show();

    $.get({
        url: route,
        complete: ()=>{
            // loadingDiv.addClass('d-none').removeClass('d-flex');
            Spinner.hide();
        },
        success: (resp)=>{
            const respContainer = $('<div>').append(resp);

            if(respContainer.find('#photo-list-container').length > 0){ //si on a des photos -> charger uniquement la grille
                loadHtml(resp, '#photo-list-container');
            } else { // sinon, cela veut dire qu'on a pa la droit de voir les photos -> charger le contenu complet
                loadHtml(resp, '#profileContentContainer');
            }

            // si le chargement de la page n'est pas fait en arrière-plan, alors mettre a jour l'historique et l'URL
            if(!inBackground) {
                currentPage = page;
                const respContainer = $('<div>').append(resp);
                const pageTitle = respContainer.find('title').text();

                document.title = pageTitle;
                window.history.pushState({
                    "body": respContainer.find('#body').html(),
                    "pageTitle": pageTitle,
                    "profilePageNumber": page,
                }, "", route);
            }
        },
        statusCode: {
            403: ()=>{
                new GenericAlert({
                    message: "Vous n'avez pas l'autorisation de voir les photos demandées",
                    buttonText: "Recharger la page",
                    onHide: () => {
                        window.location.reload(false);
                    },
                });
            },
            404: ()=> {
                new GenericAlert({
                    message: "Les photos demandées n'existent pas",
                    buttonText: "Recharger la page",
                    onHide: () => {
                        window.location.reload(false);
                    },
                });
            }
        }
    });
}


function loadPhotoDetail(photoId){
    const photoDetailContainer = $('#photoDetailContainer');
    const modalPhotoDetail = $('#modalShowPhoto');

    Spinner.show();

    $.get({
        url: Routing.generate('photo.get', {photo_id: photoId}),
        complete: ()=>{
            Spinner.hide();
        },
        success : (resp) => {
            photoDetailContainer.html(resp);
            modalPhotoDetail.modal('show');

            //mettre a jour l'url une fois la photo affichée
            const route = Routing.generate('profile.display.photo', {
                pseudo: profilPseudo,
                photo: photoId
            });
            window.history.pushState({
                "body": $('#body').html(),
                "pageTitle": document.title,
                profilePageNumber: currentPage,
                modal: '#modalShowPhoto',
            }, "", route);
        },
        statusCode: {
            403: () => {
                new GenericAlert({
                    message: "Vous n'avez pas l'autorisation de voir la photo demandée",
                    buttonText: "Ah zut",
                });
            },
            404: () => {
                new GenericAlert({
                    message: "La photo demandée n'existe pas",
                    buttonText: "Ah bah mince !",
                });
            }
        }
    });
}

/**
 * Sert à bind et unbind le "history.back()" automatique lorsque l'on quitte l'affichage d'une photo.
 * Permet d'éviter de back() lors de l'affichage d'un formulaire ou d'une modal
 * @type {{backOnHide: {bind: ModalPhotoDetailBinder.backOnHide.bind, func: ModalPhotoDetailBinder.backOnHide.func, unbind: ModalPhotoDetailBinder.backOnHide.unbind}, selector: string}}
 * @author Joris Baron
 */
const ModalPhotoDetailBinder = {
    selector: '#modalShowPhoto',
    backOnHide: {
        func: function(){
            window.history.back();
        },
        bind: function(){
            $('body').on('hidden.bs.modal', ModalPhotoDetailBinder.selector, this.func);
        },
        unbind: function(){
            $('body').off('hidden.bs.modal', ModalPhotoDetailBinder.selector, this.func);
        }
    },
}

/**
 * @author Maurane Glaude
 */
bodyElm.on("click", "#sort-photo-btn", function(){
    let order = $(this).children().data('order');

    $.get({
        url: Routing.generate('photo.grid.get', {
            pseudo: profilPseudo,
            page: currentPage,
            order: order,
        }),
        success: function(html) {
            $("#photo-row").html(html);
        }
    })
});

/**
 * @author Maurane Glaude
 */
bodyElm.on('submit', '#filter-form', function(e){
    e.preventDefault();

    let filterForm = $("#filter-form");
    const formData = filterForm.serialize();
    Spinner.show()

    $.post({
        url: Routing.generate('photo.grid.get', {
            pseudo: profilPseudo,
            page: 1,
            order: 'ASC',
        }),
        data: formData,
        complete: function () {
            Spinner.hide();
        },
        success: function(html) {
            $("#photo-row").html(html);
        }
    });
});
