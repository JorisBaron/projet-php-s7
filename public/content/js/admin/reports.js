/**
 * @author Joris Baron
 */
$(function(){
	const modalReport = $('#reportModal');
	const modalReportBody = $('#reportModal .modal-body');
	const reportListContainer = $('#report-list-container');
	const reportList = $('#report-list-container ul');


	let lockModified = null;
	let dontRemoveLock = false;
	$('body').on('click', '.card-list .card:not(.card-success)', function(){
		Spinner.show();
		const reportId = $(this).attr('data-report');

		$.get({
			url: Routing.generate('reports.get-view', {reportId: reportId}),
			success: (resp)=>{
				modalReportBody.html(resp);
				modalReport.attr('data-report',reportId);

				$.post({
					url: Routing.generate('reports.add-lock', {reportId: reportId}),
					complete: ()=>{
						Spinner.hide();
					},
					success: ()=>{
						modalReport.modal('show');
						lockModified = reportId;
					},
					error:()=>{
						lockModified = null;
						new GenericAlert({
							message: "Impossible de mettre à jour le statut du signalement.<br>" +
								"Vous pouvez tout de même continuer et traiter ce signalement",
							buttonText: "OK",
							onHide: () => modalReport.modal('show')
						})
					}
				})
			},
			error:()=>{
				Spinner.hide();
				new GenericAlert({
					message:"Erreur lors de la récupération des informations du signalement",
					buttonText:"Rafraichir la page",
					onHide:()=>{
						Spinner.show();
						window.location.reload(false);
					}
				})
			}
		})
	});

	modalReport.on('hide.bs.modal', function(){
		if(lockModified !== null && !dontRemoveLock){
			$.post(Routing.generate('reports.remove-lock', {reportId: lockModified}));
		}
	})

	//boutons suppression report et suppression photo
	modalReport
		.on('click', '#closeReport', function(){
			dontRemoveLock = true;
			modalReport.modal('hide');
			new GenericConfirmation({
				message : "Êtes-vous sûr de vouloir clôturer ce signalement <strong>sans supprimer la photo</strong> ?",
				buttonConfirmText:"Oui, certain",
				buttonConfirmColorClass: 'success',
				onConfirm : closeReport,
				buttonCancelText: "Non, finalement je vais encore y réfléchir",
				buttonCancelColorClass : 'primary',
				onCancel: ()=>{
					dontRemoveLock = false;
					modalReport.modal('show');
				}
			});
		})
		.on('click', '#deletePhoto', function(){
			dontRemoveLock = true;
			modalReport.modal('hide');
			new GenericConfirmation({
				message : "Êtes-vous sûr de vouloir <strong>supprimer la photo</strong> ?",
				buttonConfirmText:"Oui, certain",
				buttonConfirmColorClass: 'danger',
				onConfirm : deletePhoto,
				buttonCancelText: "Non, finalement je vais encore y réfléchir",
				buttonCancelColorClass : 'primary',
				onCancel: ()=>{
					dontRemoveLock = false;
					modalReport.modal('show');
				}
			});
		});

	function closeReport(){
		Spinner.show();
		$.ajax({
			method: 'DELETE',
			url: Routing.generate('reports.close', {reportId: modalReport.attr('data-report')}),
			complete: () => {Spinner.hide();},
			success:()=>{
				new GenericAlert({
					message : "Le signalement a bien été clôturé !",
					buttonText:"Merci !",
					onHide: ()=>{
						lockModified = null;
						dontRemoveLock = false;
					}
				})
			},
			error: ()=>{
				new GenericAlert({
					message : "<strong>Impossible de clore ce signalement.</strong> Tentez de rafraichir la page.",
					onHide: ()=>{
						dontRemoveLock = false;
						modalReport.modal('show');
					}
				})
			}
		})
	}

	function deletePhoto(){
		Spinner.show();
		const photoId = $('#photoDetailPhoto').attr('data-photo-id');
		$.ajax({
			method: 'DELETE',
			url: Routing.generate('photo.delete', {photo_id: photoId}),
			complete: () => {Spinner.hide();},
			success:()=>{
				new GenericAlert({
					message : "La photo a bien été supprimée !",
					buttonText:"Merci !",
					onHide: ()=>{
						lockModified = null;
						dontRemoveLock = false;
					}
				})
			},
			error: ()=>{
				new GenericAlert({
					message : "<strong>Impossible de supprimer cette photo.</strong> Tentez de rafraichir la page.",
					onHide: ()=>{
						dontRemoveLock = false;
						modalReport.modal('show');
					}
				})
			}
		})
	}

	// Bouton de refresh de liste
	$('#reloadList').on('click', function(){
		Refresher.start(true);
	})



	//gestion des "lock" des reports
	$(window)
		// envoie d'une requete d'unlock au déchangement de la page, sans attendre de réponse
		.on('pagehide', function(){
			if(lockModified !== null){
				navigator.sendBeacon(Routing.generate('reports.remove-lock', {reportId: lockModified}));
			}
		})
		// au chargement de la page, envoi d'une requete de lock si un report l'est toujours (cas de la réouverture de navigateur sur mobile)
		.on('pageshow', function(){
			if(lockModified !== null){
				$.post(Routing.generate('reports.add-lock', {reportId: lockModified}));
			}
		});


	Refresher.start();
})

const Refresher = new function(){
	this.currentRequest = null;
	this.doRefresh = true;

	const softReload = () => {
		return $.get({
			url:Routing.generate('reports.get-list'),
			success: (resp) => {
				const idList = resp.map((reportedPhoto)=> reportedPhoto.id)
				$('#report-list-container .card').each(function(){
					const indexInList = idList.indexOf(Number($(this).attr('data-report')));
					$(this).removeClass('card-danger card-warning card-success');
					if(indexInList > -1){
						if(resp[indexInList].statut > 0){
							$(this).addClass('card-warning');
						} else {
							$(this).addClass('card-danger');
						}
						$(this).find('.counter').text(resp[indexInList].count)
					} else {
						$(this).addClass('card-success');
					}
				})
			}
		});

	}

	const fullReload = () => {
		return $.get({
			url : Routing.generate('reports.get-list-view'),
			success: resp => $('#report-list-container').html(resp),
		})
	}

	const refresh = (forceFull = false) => {
		//si la liste n'est pas vide et qu'on ne force pas au full reload -> soft reload
		if($('li#empty-list').length === 0 && !forceFull) {
			this.currentRequest = softReload()
		} else { // sinon -> full reload
			this.currentRequest = fullReload();
		}
		this.currentRequest.always(()=>{
			this.currentRequest = null;
			if(this.doRefresh) {
				setTimeout(refresh, 1000);
			}
		})
		return this.currentRequest;
	};



	this.start = function(forceFull = false){
		this.stop();

		this.doRefresh = true;
		const refreshPromise = refresh(forceFull);
		if(forceFull){
			Spinner.show();
			refreshPromise.always(()=>Spinner.hide());
		}
		return this;
	}

	this.stop = function(){
		this.doRefresh = false;
		if(this.currentRequest !== null){
			this.currentRequest.abort();
		}
		return this;
	}
}