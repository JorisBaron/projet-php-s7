$(document).ready(function(){
	//////////////////
	//              //
	//  DARK THEME  //
	//              //
	//////////////////
	const btnTheme = $('#btn-theme');
	const themeConfig = new ThemeConfig()

	btnTheme.on('click', function(){
		themeConfig.switchTheme();
	})

	function updateIconDarkTheme(theme){
		const dark = theme === 'dark';
		btnTheme.find('i').toggleClass('fas', !dark).toggleClass('far', dark);
	}

	themeConfig.themeChangeHandlers.push(updateIconDarkTheme);
	themeConfig.themeChangeHandlers.push(function(theme){
		const dark = theme === 'dark';
		$('.navbar').toggleClass('navbar-light', !dark).toggleClass('navbar-dark',dark);
	});
	themeConfig.initTheme();
});

"use strict";
const ThemeConfig = /** @class */ (function () {
	function ThemeConfig() {
		this.themeChangeHandlers = [];
	}
	ThemeConfig.prototype.loadTheme = function () {
		return Cookies.get('theme');
	};
	ThemeConfig.prototype.saveTheme = function (theme) {
		if (theme === null) {
			Cookies.remove('theme');
		} else {
			Cookies.set('theme', theme, {sameSite: 'strict'});
		}
	};
	ThemeConfig.prototype.initTheme = function () {
		this.displayTheme(this.getTheme());
	};
	ThemeConfig.prototype.detectTheme = function () {
		return window.matchMedia('(prefers-color-scheme: dark)').matches ? 'dark' : 'light';
	};
	ThemeConfig.prototype.getTheme = function () {
		return this.loadTheme() || this.detectTheme();
	};
	ThemeConfig.prototype.setTheme = function (theme) {
		this.saveTheme(theme);
		this.displayTheme(theme);
	};
	ThemeConfig.prototype.switchTheme = function() {
		this.setTheme(this.getTheme() === 'dark' ? 'light':'dark');
	};
	ThemeConfig.prototype.displayTheme = function (theme) {
		document.body.setAttribute('data-theme', theme);
		let _i = 0, _a = this.themeChangeHandlers;
		for (; _i < _a.length; _i++) {
			const handler = _a[_i];
			handler(theme);
		}
	};
	return ThemeConfig;
}());
