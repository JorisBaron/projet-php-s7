$(function(){
	$('.btn-remove-ami').on('mouseenter', function() {
		$(this).removeClass('btn-outline-success').addClass('btn-outline-danger')
		       .children('i').removeClass('fa-user-check').addClass('fa-user-times')
	}).on('mouseleave', function() {
		$(this).removeClass('btn-outline-danger').addClass('btn-outline-success')
		       .children('i').removeClass('fa-user-times').addClass('fa-user-check')
	}).on('click', function() {
		const pseudo = $(this).closest('.card').attr('data-pseudo').trim();
		$('body').trigger('request.remove.ami', [USER_PSEUDO, pseudo]);
	})

	$('.btn-cancel-ami').on('mouseenter', function() {
		$(this).removeClass('btn-outline-primary').addClass('btn-outline-danger')
		       .children('i').removeClass('fa-user-clock').addClass('fa-user-times')
	}).on('mouseleave', function() {
		$(this).removeClass('btn-outline-danger').addClass('btn-outline-primary')
		       .children('i').removeClass('fa-user-times').addClass('fa-user-clock')
	}).on('click', function() {
		const pseudo = $(this).closest('.card').attr('data-pseudo').trim();
		$('body').trigger('request.cancel.ami', [USER_PSEUDO, pseudo]);
	})

	$('body').on('success.remove.ami success.cancel.ami', function(e, pseudo1, pseudo2){
		// on mets le pseudo de l'ami toujours en deuxième
		if(pseudo2 === USER_PSEUDO) {
			pseudo2 = pseudo1;
		}
		(new RemoveAnimator($('.card[data-pseudo="'+pseudo2+'"]').parent(), 500)).run();
	})

	$('main .card').each(function(){
		const pseudoElm = $(this).find('.card-title');
		if(isOverflown(pseudoElm[0], 'x')){
			pseudoElm.tooltip({title: pseudoElm.text()});
		}

		const nomElm = $(this).find('.card-subtitle');
		if(nomElm.length > 0 && isOverflown(nomElm[0])){
			nomElm.tooltip({title: nomElm.text()});
		}
	})
})

function isOverflown(element, where) {
	const x = (where === 'x' || !where);
	const y = (where === 'y' || !where);
	return (y && element.scrollHeight > element.clientHeight) || (x && element.scrollWidth > element.clientWidth);
}