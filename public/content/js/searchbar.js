/**
 * Gestion de la barre de recherche
 *
 * @author Joris Baron
 * @author Elhadj Diallo
 */
$(document).ready(function(){
	const searchResultsContainer = $('#search-results-container');
	const searchBarInputGroup = searchResultsContainer.parent();
	const searchResultsLoading = $('#search-loading');
	const searchResultsDiv = $('#search-results');

	$(window).on('resize', function() {
		searchResultsContainer.css('left', ((searchBarInputGroup.outerWidth() - searchResultsContainer.outerWidth()) /2) + "px" )
	}).resize();

	let searchTO;
	let currentXHR;
	$("#search-user").on('input', function(){
		clearTimeout(searchTO);
		if(currentXHR){
			currentXHR.abort();
			currentXHR=null;
		}
		const val = $(this).val()

		if(val.length > 1) {
			searchResultsContainer.removeClass('d-none');
			searchResultsLoading.removeClass('d-none');
			searchResultsDiv.addClass('d-none');

			searchTO = setTimeout(searchUser, 750, val);
		} else {
			searchResultsContainer.addClass('d-none');
		}
	});

	function searchUser(input){
		let url;
		if(input[0] === '@'){
			input = input.slice(1);
			url = Routing.generate('ajax.search.pseudo', {pseudo: input});
		} else {
			url = Routing.generate('ajax.search', {search: input});
		}

		currentXHR = $.get({
			url : url,
			success : (resp)=>{
				searchResultsDiv.html(resp);
				searchResultsDiv.removeClass('d-none');
				searchResultsLoading.addClass('d-none');
			}
		});
	}

	$('body').on('click', function(e){
		if(!searchBarInputGroup.is(e.target) && searchBarInputGroup.has(e.target).length === 0 ){
			clearTimeout(searchTO);
			if(currentXHR){
				currentXHR.abort();
				currentXHR=null;
			}
			searchResultsContainer.addClass('d-none')
		}
	})
})