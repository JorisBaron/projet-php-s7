/**
 *
 * @param paramsOrMsg
 * @param onHide
 * @param title
 * @param buttonText
 * @constructor
 *
 * @author Joris Baron
 */
const GenericAlert = function(paramsOrMsg, onHide = null, title = null, buttonText = 'OK'){
	if(typeof paramsOrMsg !== 'object' || paramsOrMsg === null){
		paramsOrMsg = {
			message : paramsOrMsg,
			title : title,
			onHide : onHide,
			buttonText : buttonText
		}
	}

	this.param = {
		message : null,
		onHide : null,
		title : null,
		buttonText : 'OK',
		buttonColorClass : 'primary',
		buttonIcon : false,
		buttonIconType : 'fas fa-check',
	};
	for (const [key, value] of Object.entries(paramsOrMsg)) {
		if(value !== undefined){
			this.param[key] = value;
		}
	}

	if(paramsOrMsg.buttonIconType) {this.param.buttonIcon = true}

	const modalElm = $('#GenericAlertModal');
	const modalBtn = modalElm.find('button[data-dismiss="modal"]');
	const modalBtnParent = modalBtn.parent().detach();

	if(this.param.title !== null){
		modalElm.find('.modal-title').text(this.param.title);

		modalElm.find('.modal-content').append(modalBtnParent);
	} else {
		modalElm.find('.modal-body').append(modalBtnParent);
	}
	modalElm.find('.modal-header').toggle(this.param.title !== null);
	modalBtnParent.toggleClass('modal-footer', this.param.title !== null)
	              .toggleClass("d-flex", this.param.title === null);
	modalElm.find('.modal-body .content')
	        .toggleClass('mb-3', this.param.title === null)
	        .html(this.param.message);


	modalBtn.removeClass(removeBtnClasses)
	        .addClass('btn-'+this.param.buttonColorClass);
	modalBtn.find('span')
	        .text(this.param.buttonText);
	modalBtn.find('i')
	        .toggle(this.param.buttonIcon)
	        .removeClass(removeFaClasses)
	        .addClass(this.param.buttonIconType + ' ml-2');

	modalElm.off('hidden.bs.modal').one('hidden.bs.modal', this.param.onHide);

	modalElm.modal('show');
}


/**
 *
 * @param paramsOrMsg
 * @param onConfirm
 * @param onCancel
 * @param title
 * @param buttonConfirmText
 * @param buttonCancelText
 * @constructor
 *
 * @author Joris Baron
 */
const GenericConfirmation = function(paramsOrMsg, onConfirm, onCancel = null, title = null, buttonConfirmText = 'Confirmer', buttonCancelText = 'Annuler'){
	if(typeof paramsOrMsg !== 'object' || paramsOrMsg === null){
		paramsOrMsg = {
			message : paramsOrMsg,
			title : title,
			onConfirm : onConfirm,
			onCancel : onCancel,
			buttonConfirmText : buttonConfirmText,
			buttonTextCancel : buttonTextCancel,
		}
	}

	this.param = {
		message : null,
		title : null,

		onConfirm : null,
		buttonConfirmText : 'Confirmer',
		buttonConfirmColorClass : 'primary',
		buttonConfirmIcon : false,
		buttonConfirmIconType : 'fas fa-check',

		onCancel : null,
		buttonCancelText : 'Annuler',
		buttonCancelColorClass : 'danger',
		buttonCancelIcon : false,
		buttonCancelIconType : 'fas fa-check',
	};

	for (const [key, value] of Object.entries(paramsOrMsg)) {
		if(value !== undefined){
			this.param[key] = value;
		}
	}

	if(paramsOrMsg.buttonConfirmIconType) {this.param.buttonConfirmIcon = true}
	if(paramsOrMsg.buttonCancelIconType) {this.param.buttonCancelIcon = true}

	const modalElm = $('#GenericConfirmModal');
	const modalConfirmBtn = modalElm.find('button[data-confirm]');
	const modalCancelBtn = modalElm.find('button[data-dismiss="modal"]');
	const modalBtnParent = modalConfirmBtn.parent().detach();

	if(this.param.title !== null){
		modalElm.find('.modal-title').text(this.param.title);

		modalElm.find('.modal-content').append(modalBtnParent);
	} else {
		modalElm.find('.modal-body').append(modalBtnParent);
	}

	modalElm.find('.modal-header').toggle(this.param.title !== null);
	modalBtnParent.toggleClass('modal-footer', this.param.title !== null)
	              .toggleClass("d-flex justify-content-end", this.param.title === null);

	modalElm.find('.modal-body .content')
	        .toggleClass('mb-3', this.param.title === null)
	        .html(this.param.message);


	modalConfirmBtn.removeClass(removeBtnClasses)
	               .addClass('btn-'+this.param.buttonConfirmColorClass)
	               .toggleClass('ml-2', this.param.title === null);
	modalConfirmBtn.find('span').text(this.param.buttonConfirmText);
	modalConfirmBtn.find('i').toggle(this.param.buttonConfirmIcon)
	               .removeClass(removeFaClasses)
	               .addClass(this.param.buttonConfirmIconType);

	modalCancelBtn.removeClass(removeBtnClasses)
	              .addClass('btn-'+this.param.buttonCancelColorClass);
	modalCancelBtn.find('span').text(this.param.buttonCancelText);
	modalCancelBtn.find('i').toggle(this.param.buttonCancelIcon)
	              .removeClass(removeFaClasses)
	              .addClass(this.param.buttonCancelIconType);

	modalElm.off('hide.bs.modal').one('hide.bs.modal', this.param.onCancel);
	modalConfirmBtn
		.off('click')
	    .one('click', function(){
		    modalElm.off('hide.bs.modal');
		    modalElm.modal('hide');
		}).one('click',this.param.onConfirm);

	modalElm.modal('show');
}

function removeBtnClasses(i, classes) {
	return removeRegexClasses(classes, /^btn-/);
}

function removeFaClasses(i, classes){
	return removeRegexClasses(classes, /^fa/);
}

function removeRegexClasses(classes, regex) {
	return classes.split(' ').filter(function(cl) {
		return regex.test(cl);
	}).join(' ');
}