/**
 * Element permettant d'afficher un "Spinner" de chargement global sur toute la page
 * Facilite l'affichage d'un chargement
 * @author Joris Baron
 */

const Spinner = new function() {
	const selector = '#globalSpinner';
	const getElement = function(){
		return $(selector);
	}
	let nbShow = 1;

	this.hide = function(duration = 200) {
		nbShow = Math.max(nbShow-1, 0);
		if(nbShow<1){
			getElement().fadeOut(duration);
		}
	}
	this.show = function(duration = 200){
		getElement().fadeIn(duration);
		nbShow++;
	}
}


$(document).ready(function() {
	// on cache le spinner global à la fin du chargement de la page
	Spinner.hide(200);
});