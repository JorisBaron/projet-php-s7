/**
 * Supprime un ou plusieurs éléments du DOM avec une animation sympathique
 * @param elementToRemove  le ou les éléments à supprimer
 * @param duration durée de l'animation
 * @constructor
 */
const RemoveAnimator = function(elementToRemove, duration){
	this.element = $(elementToRemove);
	this.duration = duration;

	this.run = function(){
		const nextElm = this.element.nextAll();
		const finalOffsets = [];

		nextElm.each(function(){
			finalOffsets.push($(this).offset());
		});

		//permet d'avoir le future offset des éléments, après suppression
		this.element.hide();
		nextElm.each(function(i){
			const thisOffset = $(this).offset();
			finalOffsets[i].top = thisOffset.top - finalOffsets[i].top;
			finalOffsets[i].left = thisOffset.left - finalOffsets[i].left;
		});
		this.element.show();

		const duration = this.duration;
		nextElm.each(function(i){
			$(this).animate(finalOffsets[i], duration);
		})
		this.element
		    .css('z-index','0')
		    .fadeOut(this.duration, function(){
			    $( this ).remove();
			    nextElm.each(function(i){
				    $(this).stop();
				    $(this).css({top:0,left:0});
			    })
		    });
	}
}