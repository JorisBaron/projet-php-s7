/**
 * JS de la page de profil.
 * Gestion des boutons d'ajout/suppression/etc d'amis de la partie haute du profil
 * @file base.js
 * @author Joris Baron
 */

$(function() {
	const btnAmiContainer = $('#btn-profil-container'),
		btnAmiAdd = $('#btn-ami-add'),
		btnAmiCancel = $('#btn-ami-cancel'),
		btnAmiAccept = $('#btn-ami-accept'),
		btnAmiDeny = $('#btn-ami-deny'),
		btnAmiRemove = $('#btn-ami-remove');


	btnAmiContainer.find('[data-toggle="tooltip"]').tooltip()

	btnAmiAdd.on('click', function() {
		$('body').trigger('request.add.ami', [USER_PSEUDO, profilPseudo]);
	});

	btnAmiCancel.on('mouseenter', function() {
		$(this).removeClass('btn-outline-primary').addClass('btn-outline-danger')
		       .children('i').removeClass('fa-user-clock').addClass('fa-user-times')
	}).on('mouseleave', function() {
		$(this).removeClass('btn-outline-danger').addClass('btn-outline-primary')
		       .children('i').removeClass('fa-user-times').addClass('fa-user-clock')
	}).on('click', function() {
		$('body').trigger('request.cancel.ami', [USER_PSEUDO, profilPseudo]);
	})

	btnAmiAccept.on('mouseenter', function() {
		$(this).removeClass('btn-outline-primary').addClass('btn-outline-success')
	}).on('mouseleave', function() {
		$(this).removeClass('btn-outline-success').addClass('btn-outline-primary')
	}).on('click', function() {
		$('body').trigger('request.accept.ami', [USER_PSEUDO, profilPseudo]);
	})

	btnAmiDeny.on('mouseenter', function() {
		$(this).removeClass('btn-outline-primary').addClass('btn-outline-danger')
	}).on('mouseleave', function() {
		$(this).removeClass('btn-outline-danger').addClass('btn-outline-primary')
	}).on('click', function() {
		$('body').trigger('request.deny.ami', [USER_PSEUDO, profilPseudo]);
	})

	btnAmiRemove.on('mouseenter', function() {
		$(this).removeClass('btn-outline-success').addClass('btn-outline-danger')
		       .children('i').removeClass('fa-user-check').addClass('fa-user-times')
	}).on('mouseleave', function() {
		$(this).removeClass('btn-outline-danger').addClass('btn-outline-success')
		       .children('i').removeClass('fa-user-times').addClass('fa-user-check')
	}).on('click', function() {
		$('body').trigger('request.remove.ami', [USER_PSEUDO, profilPseudo]);
	})

	$('body').on('success.add.ami', function(e, pseudo1, pseudo2){
		if(pseudo2 === profilPseudo) {
			btnAmiContainer.children().addClass('d-none');
			btnAmiCancel.removeClass('d-none')
		}
	}).on('success.cancel.ami success.deny.ami success.remove.ami', function(e, pseudo1, pseudo2){
		if(pseudo2 === profilPseudo) {
			btnAmiContainer.children().addClass('d-none');
			btnAmiAdd.removeClass('d-none')
		}
	}).on('success.accept.ami', function(e, pseudo1, pseudo2){
		if(pseudo2 === profilPseudo) {
			btnAmiContainer.children().addClass('d-none');
			btnAmiRemove.removeClass('d-none')
		}
	})
})