$(document).ready(function(){

	$(document).on('click', '#btn-notif-amis-container .dropdown-menu', function (e) {
		e.stopPropagation();
	});


	$('#btn-notif-amis-container button[data-action="accept"]')
		.on('click', function(e){
			e.stopPropagation();
			e.preventDefault();

			const pseudo = $(this).closest('a.dropdown-item').attr('data-pseudo');
			$('body').trigger('request.accept.ami', [USER_PSEUDO, pseudo]);
		})
		.on('mouseenter', function() {
			$(this).removeClass('btn-outline-primary').addClass('btn-outline-success')
		})
		.on('mouseleave', function() {
			$(this).removeClass('btn-outline-success').addClass('btn-outline-primary')
		});

	$('#btn-notif-amis-container button[data-action="deny"]')
		.on('click', function(e){
			e.stopPropagation();
			e.preventDefault();

			const pseudo = $(this).closest('a.dropdown-item').attr('data-pseudo');
			$('body').trigger('request.deny.ami', [USER_PSEUDO, pseudo]);
		})
		.on('mouseenter', function() {
			$(this).removeClass('btn-outline-primary').addClass('btn-outline-danger')
		})
		.on('mouseleave', function() {
			$(this).removeClass('btn-outline-danger').addClass('btn-outline-primary')
		});


	$('body')
		// Demande en ami
		.on('request.add.ami', function(e, pseudo1, pseudo2){
			new GenericConfirmation({
				title: "Demander en ami",
				message: "Êtes-vous sûr de vouloir demander @"+pseudo2+" en ami ?",
				onConfirm: () => addAmi(pseudo1, pseudo2),
				buttonConfirmColorClass: 'success',
				buttonCancelColorClass: 'primary'
			});
		}).on('success.add.ami', function(e, pseudo1, pseudo2){
			new GenericAlert({
				message: "La demande en ami a bien été envoyée à @" + pseudo2+".",
				buttonText: 'Parfait !'
			});
		}).on('error.add.ami', function(){
			new GenericAlert({
				message : "La demande en ami n'a pas pu être envoyée.",
				buttonText : 'Ah dommage, je vais réessayer alors',
				buttonColorClass: 'danger'
			});
		})
		// Acceptation de demande
		.on('request.accept.ami', function(e, pseudo1, pseudo2){
			new GenericConfirmation({
				title: "Accepter la demande d'ami",
				message: "Voulez-vous devenir ami avec @"+pseudo2+" ?",
				onConfirm: () => acceptAmi(pseudo1, pseudo2),
				buttonConfirmColorClass: 'success',
				buttonCancelColorClass: 'primary'
			});
		}).on('success.accept.ami', function(e, pseudo1, pseudo2){
			new GenericAlert({
				message: "Vous êtes désormais amis avec @" + pseudo2,
				buttonText: 'Super !',
				onHide: ()=>removeNotifDemandeAmi(pseudo2)
			});
		}).on('error.accept.ami', function(e, pseudo1, pseudo2){
			new GenericAlert({
				message: "Erreur lors de l'acceptation de la demande. La demande n'a pas pu être acceptée.",
				buttonText: 'Ah dommage, je vais réessayer alors',
			});
		})
		// Refus de demande
		.on('request.deny.ami', function(e, pseudo1, pseudo2){
			new GenericConfirmation({
				title:"Refuser la demande d'ami",
				message: "Êtes-vous sûr de vouloir refuser la demande d'ami de @"+pseudo2+" ?",
				onConfirm: () => denyAmi(pseudo1, pseudo2),
				buttonConfirmText : "Oui, cette personne m'importune !",
				buttonConfirmColorClass: 'danger',
				buttonCancelText : "Je suis pas si sûr en fait...",
				buttonCancelColorClass: 'primary'
			});
		}).on('success.deny.ami', function(e, pseudo1, pseudo2){
			new GenericAlert({
				message: "La demande en ami de @"+pseudo2+" a été refusée.",
				buttonText: 'Merci bien',
				onHide: ()=>removeNotifDemandeAmi(pseudo2)
			});
		}).on('error.deny.ami', function(e, pseudo1, pseudo2){
			new GenericAlert({
				message: "Erreur lors de l'acceptation de la demande. La demande n'a pas pu être acceptée.",
				buttonText: 'Ah, je vais réessayer de ce pas !',
			});
		})
		// Annulation de demande
		.on('request.cancel.ami', function(e, pseudo1, pseudo2){
			new GenericConfirmation({
				title:"Annuler la demande d'ami",
				message: "Êtes-vous sûr de vouloir annuler la demande en ami ?",
				onConfirm: () => cancelAmi(pseudo1, pseudo2),
				buttonConfirmText : "Oui, en fait je n'ai plus envie",
				buttonConfirmColorClass: 'danger',
				buttonCancelText : "Je vais peut-être attendre encore un peu finalement",
				buttonCancelColorClass: 'primary'
			});
		}).on('success.cancel.ami', function(e, pseudo1, pseudo2){
			new GenericAlert({
				message: "L'annulation de la demande a bien été effectuée.",
				buttonText: 'Nickel !',
			});
		}).on('error.cancel.ami', function(e, pseudo1, pseudo2){
			new GenericAlert({
				message: "Erreur lors de l'acceptation de la demande. La demande n'a pas pu être acceptée.",
				buttonText: 'Ah, mince, je vais réessayer',
			});
		})
		// Suppression des amis
		.on('request.remove.ami', function(e, pseudo1, pseudo2){
			new GenericConfirmation({
				title:'Supprimer de mes amis',
				message: "Êtes-vous sûr de vouloir supprimer @"+pseudo2+" de votre liste d'amis ?",
				onConfirm: () => removeAmi(pseudo1, pseudo2),
				buttonConfirmText : "Oui, je ne veux plus de cette personne dans mes amis !",
				buttonConfirmColorClass: 'danger',
				buttonCancelText : "Non finalement je l'aime bien",
				buttonCancelColorClass: 'primary'
			});
		}).on('success.remove.ami', function(e, pseudo1, pseudo2){
			new GenericAlert({
				message: "@"+pseudo2+" a bien été retiré(e) de votre liste d'amis.",
				buttonText: 'Parfait !',
			});
		}).on('error.remove.ami', function(e, pseudo1, pseudo2){
			new GenericAlert({
				message: "Erreur lors de la suppression de la liste d'amis. Merci de réessayer.",
				buttonText: 'Ah ! je vais réessayer tout de suite',
			});
		});


	function removeNotifDemandeAmi(pseudo){
		$('#btn-notif-amis-container a[data-pseudo="'+pseudo+'"]').remove();
		const noNotif = $('#no-notif-text');
		if($('#list-notif').children().not(noNotif).length === 0){
			noNotif.removeClass('d-none');
			$('#badge-notifs').remove();
		}
	}


	function genericRemoveAmi(pseudo1, pseudo2, eventType) {
		Spinner.show();
		return $.post({
			url: BASEURL + 'ajax/ami/delete/' + pseudo1 + '/' + pseudo2,
			// data: {user: userPseudo, ami: profilPseudo},
			complete: ()=>{
				Spinner.hide();
			},
			success:function(resp) {
				if(resp) {
					$('body').trigger('success.'+eventType+'.ami', [pseudo1, pseudo2]);
				} else {
					$('body').trigger('error.'+eventType+'.ami', [pseudo1, pseudo2]);
				}
			},
			error: function(){
				$('body').trigger('error.'+eventType+'.ami', [pseudo1, pseudo2]);
			}
		})
	}

	function cancelAmi(pseudo1, pseudo2) {
		return genericRemoveAmi(pseudo1, pseudo2, 'cancel', pseudo1, pseudo2);
	}

	function denyAmi(pseudo1, pseudo2) {
		return genericRemoveAmi(pseudo1, pseudo2, 'deny', pseudo1, pseudo2);
	}

	function removeAmi(pseudo1, pseudo2) {
		return genericRemoveAmi(pseudo1, pseudo2, 'remove', pseudo1, pseudo2);
	}

	function addAmi(pseudo1, pseudo2) {
		Spinner.show();
		return $.post({
			url: BASEURL + 'ajax/ami/add/' + pseudo1 + '/' + pseudo2,
			complete: ()=>{
				Spinner.hide();
			},
			success:function(resp) {
				if(resp) {
					$('body').trigger('success.add.ami', [pseudo1, pseudo2]);
				} else {
					$('body').trigger('error.add.ami', [pseudo1, pseudo2]);
				}
			},
			error: function(){
				$('body').trigger('error.add.ami', [pseudo1, pseudo2]);
			}
		})
	}

	function acceptAmi(pseudo1, pseudo2) {
		Spinner.show();
		return $.post({
			url: BASEURL + 'ajax/ami/accept/' + pseudo1 + '/' + pseudo2,
			complete: ()=>{
				Spinner.hide();
			},
			success:function(resp) {
				if(resp) {
					$('body').trigger('success.accept.ami', [pseudo1, pseudo2]);
				} else {
					$('body').trigger('error.accept.ami', [pseudo1, pseudo2]);
				}
			},
			error: function (){
				$('body').trigger('error.accept.ami', [pseudo1, pseudo2]);
			}
		})
	}

})