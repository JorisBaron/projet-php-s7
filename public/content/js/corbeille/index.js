/**
 * Pour le fichier entier :
 * @author Joris Baron
 */

$(function(){
	const modalPhoto = $('#modalPhoto');

	$('[data-toggle="modal-photo"]').on('click', function(e){
		Spinner.show();

		const id = $(this).closest('.card').attr('data-id-photo');
		modalPhoto.attr('data-id-photo', id);


		$.get({
			url: Routing.generate('corbeille.get-photo', {photo: id}),
			complete: () => {
				Spinner.hide();
			},
			success: (resp) => {
				modalPhoto.find('.modal-body').html(resp);
				modalPhoto.modal('show');
			}
		})
	});

	modalPhoto.on('click', '.restore-photo', function(){
		const id = modalPhoto.attr('data-id-photo');
		restorePhoto(id);
	});
	modalPhoto.on('click', '.delete-photo', function(){
		const id = modalPhoto.attr('data-id-photo');
		deletePhoto(id);
	});

	$('main.container .card button.restore-photo').on('click', function(){
		const id = $(this).closest('.card').attr('data-id-photo');
		restorePhoto(id);
	});
	$('main.container .card button.delete-photo').on('click', function(){
		const id = $(this).closest('.card').attr('data-id-photo');
		deletePhoto(id);
	});


	function restorePhoto(id){
		Spinner.show();
		modalPhoto.modal('hide');

		const cardContainer = $('.card[data-id-photo="'+id+'"]').parent()

		$.post({
			url: Routing.generate('photo.restore',{photo_id: id}),
			complete: () => {
				Spinner.hide();
			},
			success: function(resp){
				genericAlertWithRemoval("La photo a bien été restaurée !", cardContainer, 'Merci !');
			},
			statusCode:{
				401: ()=>{
					generic401Handler("Vous ne pouvez pas restaurer cette photo car vous n'êtes pas connecté",
						"Ah mince, je vais aller me connecter alors");
				},
				403: ()=>{
					new GenericAlert("Vous n'avez pas l'autorisation de supprimer cette photo", null, null, 'Zut, je me suis trompé');
				},
				404: ()=>{
					genericAlertWithRemoval("La photo à supprimer n'existe pas", cardContainer, 'Ah bah mince alors');
				}
			}
		})
	}

	function deletePhoto(id){
		Spinner.show();
		modalPhoto.modal('hide');

		const cardContainer = $('.card[data-id-photo="'+id+'"]').parent()

		$.ajax({
			method: 'DELETE',
			url: Routing.generate('photo.delete',{photo_id: id}),
			complete: () => {
				Spinner.hide();
			},
			success: function(resp){
				genericAlertWithRemoval("La photo a bien été supprimée définitivement !", cardContainer, 'Merci !');
			},
			statusCode:{
				401: ()=>{
					generic401Handler("Vous ne pouvez pas supprimer de photo car vous n'êtes pas connecté",
						"Ah mince, je vais aller me connecter alors");
				},
				403: ()=>{
					new GenericAlert("Vous n'avez pas l'autorisation de supprimer cette photo", null, null, 'Zut, je me suis trompé');
				},
				404: ()=>{
					genericAlertWithRemoval("La photo à supprimer n'existe pas", cardContainer, 'Ah bah mince alors');
				}
			}
		})
	}
})



function generic401Handler(message, buttonText){
	new GenericAlert(message, () => {
		Spinner.show();
		window.location.replace(
			Routing.generate('app_login', null, true)
		);
	}, undefined, buttonText);
}

function genericAlertWithRemoval(message, elmToDelete, buttonText){
	new GenericAlert(message, ()=>{
		const RA = new RemoveAnimator(elmToDelete, 500);
		RA.run();
	}, undefined, buttonText);
}