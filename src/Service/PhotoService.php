<?php


namespace App\Service;


use App\Repository\PhotoRepository;
use App\Repository\UserRepository;

class PhotoService {
	private $photoRepository;
	private $userRepository;

	public function __construct(PhotoRepository $photoRepository, UserRepository $userRepository) {
		$this->photoRepository = $photoRepository;
		$this->userRepository = $userRepository;
	}

	/**
	 * Génère une nom de fichier aléatoire pour une photo
	 * @param string $extension
	 * @return string
	 *
	 * @author Joris Baron
	 */
	private function generateHash(string $extension): string {
		return uniqid("",true).uniqid("",true).'.'.$extension;
	}

	/**
	 * Génère une nom de fichier aléatoire pour une photo, en vérifiant qu'il n'existe pas dans la BDD des photos
	 * @param string $extension
	 * @return string
	 *
	 * @author Joris Baron
	 */
	public function generateHashForPhoto(string $extension): string {
		do {
			$photoFilename = $this->generateHash($extension);
		} while($this->photoRepository->findOneByFichier($photoFilename) !== null);
		return $photoFilename;
	}

	/**
	 * Génère une nom de fichier aléatoire pour une photo de profile, en vérifiant qu'il n'existe pas dans la BDD des users
	 * @param string $extension
	 * @return string
	 *
	 * @author Joris Baron
	 */
	public function generateHashForUser(string $extension): string {
		do {
			$photoFilename = $this->generateHash($extension);
		} while($this->userRepository->findOneByPhoto($photoFilename) !== null);
		return $photoFilename;
	}
}