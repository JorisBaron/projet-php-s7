<?php

namespace App\Entity;

use App\Repository\UserRepository;
use DateTime;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @UniqueEntity(fields={"pseudo"}, message="Ce pseudo est déjà pris !")
 */
class User extends GenericUser /*implements UserInterface*/
{
    ///**
    // * @ORM\Id
    // * @ORM\GeneratedValue
    // * @ORM\Column(type="integer")
    // */
    //private $id;

    ///**
    // * @ORM\Column(type="string", length=180, unique=true)
    // */
    //private $email;
	//
    ///**
    // * @ORM\Column(type="json")
    // */
    //private $roles = [];
	//
    ///**
    // * @var string The hashed password
    // * @ORM\Column(type="string")
    // */
    //private $password;
	//
    ///**
    // * @ORM\Column(type="string", length=255, nullable=true)
    // */
    //private $nom;
	//
    ///**
    // * @ORM\Column(type="string", length=255, nullable=true)
    // */
    //private $prenom;

    /**
     * @ORM\Column(type="string", length=50, unique=true)
     */
    private $pseudo;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $photo;

    /**
     * @ORM\Column(type="date")
     */
    private $date_naissance;

    /**
     * @ORM\Column(type="date")
     */
    private $date_inscription;

    /**
     * @ORM\Column(type="boolean", options={"default" : false})
     */
    private $public;

    /**
     * @ORM\OneToMany(targetEntity=Album::class, mappedBy="user", orphanRemoval=true)
     */
    private $albums;

    /**
     * @ORM\OneToMany(targetEntity=Photo::class, mappedBy="user", orphanRemoval=true)
     */
    private $photos;

    /**
     * @ORM\OneToMany(targetEntity=Relation::class, mappedBy="user1", orphanRemoval=true, cascade={"persist", "remove"})
     */
    private $relationsSortantes;

    /**
     * @ORM\OneToMany(targetEntity=Relation::class, mappedBy="user2", orphanRemoval=true, cascade={"persist", "remove"})
     */
    private $relationsEntrantes;

    /**
     * @ORM\OneToMany(targetEntity=Report::class, mappedBy="user", orphanRemoval=true)
     */
    private $reports;


	public function __construct()
                           	{
                           		$this->date_inscription = new DateTime();
                           		$this->public = false;
                           		$this->albums = new ArrayCollection();
                           		$this->photos = new ArrayCollection();
                           		$this->relationsSortantes = new ArrayCollection();
                           		$this->relationsEntrantes = new ArrayCollection();
                             $this->reports = new ArrayCollection();
                           	}

    //public function getId(): ?int
    //{
    //    return $this->id;
    //}
	//
    //public function getEmail(): ?string
    //{
    //    return $this->email;
    //}
	//
    //public function setEmail(string $email): self
    //{
    //    $this->email = $email;
	//
    //    return $this;
    //}
	//
    ///**
    // * A visual identifier that represents this user.
    // *
    // * @see UserInterface
    // */
    //public function getUsername(): string
    //{
    //    return (string) $this->email;
    //}
	//
    ///**
    // * @see UserInterface
    // */
    //public function getRoles(): array
    //{
    //    $roles = $this->roles;
    //    // guarantee every user at least has ROLE_USER
    //    $roles[] = 'ROLE_USER';
	//
    //    return array_unique($roles);
    //}
	//
    //public function setRoles(array $roles): self
    //{
    //    $this->roles = $roles;
	//
    //    return $this;
    //}
	//
    ///**
    // * @see UserInterface
    // */
    //public function getPassword(): string
    //{
    //    return (string) $this->password;
    //}
	//
    //public function setPassword(string $password): self
    //{
    //    $this->password = $password;
	//
    //    return $this;
    //}
	//
    ///**
    // * @see UserInterface
    // */
    //public function getSalt()
    //{
    //    // not needed when using the "bcrypt" algorithm in security.yaml
    //}
	//
    ///**
    // * @see UserInterface
    // */
    //public function eraseCredentials()
    //{
    //    // If you store any temporary, sensitive data on the user, clear it here
    //    // $this->plainPassword = null;
    //}
	//
    //public function getNom(): ?string
    //{
    //    return $this->nom;
    //}
	//
    //public function setNom(?string $nom): self
    //{
    //    $this->nom = $nom;
	//
    //    return $this;
    //}
	//
    //public function getPrenom(): ?string
    //{
    //    return $this->prenom;
    //}
	//
    //public function setPrenom(?string $prenom): self
    //{
    //    $this->prenom = $prenom;
	//
    //    return $this;
    //}

    public function getPseudo(): ?string
    {
        return $this->pseudo;
    }

    public function setPseudo(string $pseudo): self
    {
        $this->pseudo = $pseudo;

        return $this;
    }

    public function getPhoto(): ?string
    {
        return $this->photo;
    }

    public function setPhoto(?string $photo): self
    {
        $this->photo = $photo;

        return $this;
    }

    public function getDateNaissance(): ?DateTimeInterface
    {
        return $this->date_naissance;
    }

    public function setDateNaissance(DateTimeInterface $date_naissance): self
    {
        $this->date_naissance = $date_naissance;

        return $this;
    }

    public function getDateInscription(): ?DateTimeInterface
    {
        return $this->date_inscription;
    }

    public function setDateInscription(DateTimeInterface $date_inscription): self
    {
        $this->date_inscription = $date_inscription;

        return $this;
    }

    public function getPublic(): ?bool
    {
        return $this->public;
    }

    public function setPublic(bool $public): self
    {
        $this->public = $public;

        return $this;
    }

    /**
     * @return Collection|Album[]
     */
    public function getAlbums(): Collection
    {
        return $this->albums;
    }

    public function addAlbum(Album $album): self
    {
        if (!$this->albums->contains($album)) {
            $this->albums[] = $album;
            $album->setUser($this);
        }

        return $this;
    }

    public function removeAlbum(Album $album): self
    {
        if ($this->albums->contains($album)) {
            $this->albums->removeElement($album);
            // set the owning side to null (unless already changed)
            if ($album->getUser() === $this) {
                $album->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Photo[]
     */
    public function getPhotos(): Collection
    {
		return $this->photos->filter(function (Photo $photo){
			return $photo->getCorbeille() === null;
		});
    }

	/**
	 * @return ArrayCollection
	 *
	 * @author Joris Baron
	 */
    public function getPhotosInCorbeille(): ArrayCollection {
    	return $this->photos->filter(function (Photo $photo){
    		return $photo->getCorbeille() !== null;
		});
	}

	/**
	 * @return Collection|Photo[]
	 */
	public function getAllPhotos(): Collection
                           	{
                           		return $this->photos;
                           	}

    public function addPhoto(Photo $photo): self
    {
        if (!$this->photos->contains($photo)) {
            $this->photos[] = $photo;
            $photo->setUser($this);
        }

        return $this;
    }

	/**
	 * @param Photo $photo
	 * @return $this
	 *
	 * @author Joris Baron
	 */
    public function throwPhotoAway(Photo $photo): self {
    	$photo->throwAway();
    	return $this;
	}

	/**
	 * @param Photo $photo
	 * @return $this
	 *
	 * @author Joris Baron
	 */
	public function getPhotoBack(Photo $photo): self {
                           		$photo->restore();
                           		return $this;
                           	}

    public function removePhoto(Photo $photo): self
    {
        if ($this->photos->contains($photo)) {
            $this->photos->removeElement($photo);
            // set the owning side to null (unless already changed)
            if ($photo->getUser() === $this) {
                $photo->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Relation[]
     */
	public function getRelationsSortantes(): Collection
                               {
                                   return $this->relationsSortantes;
                               }

	protected function addRelationsSortantes(Relation $relationsEntrante): self
                               {
                                   if (!$this->relationsSortantes->contains($relationsEntrante)) {
                                       $this->relationsSortantes[] = $relationsEntrante;
                                       $relationsEntrante->setUser1($this);
                                   }
                           
                                   return $this;
                               }

	protected function removeRelationsSortantes(Relation $relationsEntrante): self
                               {
                                   if ($this->relationsSortantes->contains($relationsEntrante)) {
                                       $this->relationsSortantes->removeElement($relationsEntrante);
                                       // set the owning side to null (unless already changed)
                                       if ($relationsEntrante->getUser1() === $this) {
                                           $relationsEntrante->setUser1(null);
                                       }
                                   }
                           
                                   return $this;
                               }

    /**
     * @return Collection|Relation[]
     */
	public function getRelationsEntrantes(): Collection
   {
	   return $this->relationsEntrantes;
   }

	/**
	 * @return Collection|Relation[]
	 *
	 * @author Joris Baron
	 */
    public function getRelationsEntrantesEnAttente(): Collection{
		return $this->getRelationsEntrantes()->filter(function (Relation $relation){
			return $relation->getStatut() == false;
		});
	}

	protected function addRelationsEntrantes(Relation $relationsEntrantes): self
                               {
                                   if (!$this->relationsEntrantes->contains($relationsEntrantes)) {
                                       $this->relationsEntrantes[] = $relationsEntrantes;
                                       $relationsEntrantes->setUser2($this);
                                   }
                           
                                   return $this;
                               }

    protected function removeRelationsEntrantes(Relation $relationsEntrantes): self
    {
        if ($this->relationsEntrantes->contains($relationsEntrantes)) {
            $this->relationsEntrantes->removeElement($relationsEntrantes);
            // set the owning side to null (unless already changed)
            if ($relationsEntrantes->getUser2()===$this) {
                $relationsEntrantes->setUser2(null);
            }
        }

        return $this;
    }

	/**
	 * @return ArrayCollection
	 *
	 * @author Joris Baron
	 */
	public function getRelations(): ArrayCollection {
                           		return new ArrayCollection(array_merge(
                           			$this->getRelationsEntrantes()->toArray(), $this->getRelationsSortantes()->toArray()
                           		));
                           	}

	/**
	 * @param User|null $user
	 * @return Relation|null
	 *
	 * @author Joris Baron
	 */
	public function getRelationFromAmi(?User $user): ?Relation {
                               	if($user!==null) {
                           			foreach($this->getRelationsSortantes() as $r) {
                           				if($r->getUser2() === $user) {
                           					return $r;
                           				}
                           			}
                           			foreach($this->getRelationsEntrantes() as $r) {
                           				if($r->getUser1() === $user) {
                           					return $r;
                           				}
                           			}
                           		}
                           		return null;
                           	}

	/**
	 * @param User $user
	 * @return bool
	 *
	 * @author Joris Baron
	 */
	public function hasDemandeEnAttente(User $user): bool {
                           		return $this->getRelationFromAmi($user)->getStatut() === false;
                           	}

	/**
	 * @param Relation $relation
	 * @return $this
	 *
	 * @author Joris Baron
	 */
	public function addRelation(Relation $relation): self {
                           		if (!$this->relationsEntrantes->contains($relation)) {
                           			$this->addRelationsSortantes($relation);
                           		}
                           
                           		return $this;
                           	}

	/**
	 * @param Relation $relation
	 * @return User
	 *
	 * @author Joris Baron
	 */
	public function removeRelation(Relation $relation): User {
                           		$this->removeRelationsEntrantes($relation);
                           		$this->removeRelationsSortantes($relation);
                           		return $this;
                           	}

	/**
	 * @return ArrayCollection
	 *
	 * @author Joris Baron
	 */
    public function getAmisAvecStatut(): ArrayCollection {
		return new ArrayCollection(array_merge(
			$this->getRelationsSortantes()->map(function (Relation $relation){
    			return ['user' => $relation->getUser1(), 'statut' => $relation->getStatut()];
			})->toArray(),
			$this->getRelationsSortantes()->map(function (Relation $relation){
				return ['user' => $relation->getUser2(), 'statut' => $relation->getStatut()];
			})->toArray()
		));
	}

	/**
	 * @return ArrayCollection
	 *
	 * @author Joris Baron
	 */
	public function getAmis(): ArrayCollection {
                           		return new ArrayCollection(array_merge(
                           			$this->getRelationsEntrantes()->map(function (Relation $relation){
                           				return $relation->getUser1();
                           			})->toArray(),
                           			$this->getRelationsSortantes()->map(function (Relation $relation){
                           				return $relation->getUser2();
                           			})->toArray()
                           		));
                           	}

	/**
	 * @param User $user
	 * @return $this
	 *
	 * @author Joris Baron
	 */
	public function demandeAmi(User $user): self {
                           		if(!$this->getAmis()->contains($user)){
                           			$relation = new Relation();
                           			$relation->setUser1($this)->setUser2($user);
                           			$this->addRelationsSortantes($relation);
                           		}
                           
                           		return $this;
                           	}

	/**
	 * @param User $user
	 * @return $this
	 *
	 * @author Joris Baron
	 */
	public function accepteAmi(User $user): self {
                           		$relation = $this->getRelationFromAmi($user);
                           		if($relation !== null && $relation->getUser2() === $this){
                           			$relation->setStatut(true);
                           		}
                           
                           		return $this;
                           	}

	/**
	 * @param User $user
	 * @return $this
	 *
	 * @author Joris Baron
	 */
	public function removeAmi(User $user): self {
                           		return $this->removeRelation($this->getRelationFromAmi($user));
                           	}

	/**
	 * @param User|null $user
	 * @param bool $accepte spécifie s'il faut que la demande soit acceptée ou non pour renvoyer true
	 * @return bool true si une relation existe et $accepte == false, ou si une relation en statut acceptée existe, false sinon
	 *
	 * @author Joris Baron
	 */
	public function isAmi(?User $user, bool $accepte = true): bool {
                               	if($user===null){
                               		return false;
                           		}
                               	// par convention, on considère qu'on est forcément ami avec soi-même
                               	if($user === $this){
                               		return true;
                           		}
                           
                           		$relation = $this->getRelationFromAmi($user);
                           		if($relation!==null){
                           			return !$accepte // $accepte == false -> peu importe le statut de la demande
                           				   || $relation->getStatut(); // true si acceptée
                           		}
                           		return false;
                           	}

    /**
     * @return Collection|Report[]
     */
    public function getReports(): Collection
    {
        return $this->reports;
    }

    public function addReport(Report $report): self
    {
        if (!$this->reports->contains($report)) {
            $this->reports[] = $report;
            $report->setUser($this);
        }

        return $this;
    }

    public function removeReport(Report $report): self
    {
        if ($this->reports->removeElement($report)) {
            // set the owning side to null (unless already changed)
            if ($report->getUser() === $this) {
                $report->setUser(null);
            }
        }

        return $this;
    }

    public function addRelationsSortante(Relation $relationsSortante): self
    {
        if (!$this->relationsSortantes->contains($relationsSortante)) {
            $this->relationsSortantes[] = $relationsSortante;
            $relationsSortante->setUser1($this);
        }

        return $this;
    }

    public function removeRelationsSortante(Relation $relationsSortante): self
    {
        if ($this->relationsSortantes->removeElement($relationsSortante)) {
            // set the owning side to null (unless already changed)
            if ($relationsSortante->getUser1() === $this) {
                $relationsSortante->setUser1(null);
            }
        }

        return $this;
    }

    public function addRelationsEntrante(Relation $relationsEntrante): self
    {
        if (!$this->relationsEntrantes->contains($relationsEntrante)) {
            $this->relationsEntrantes[] = $relationsEntrante;
            $relationsEntrante->setUser2($this);
        }

        return $this;
    }

    public function removeRelationsEntrante(Relation $relationsEntrante): self
    {
        if ($this->relationsEntrantes->removeElement($relationsEntrante)) {
            // set the owning side to null (unless already changed)
            if ($relationsEntrante->getUser2() === $this) {
                $relationsEntrante->setUser2(null);
            }
        }

        return $this;
    }
}
