<?php

namespace App\Entity;

use App\Repository\UserToDeleteRepository;
use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=UserToDeleteRepository::class)
 */
class UserToDelete
{
    ///**
    // *
    // * @ORM\GeneratedValue
    // * @ORM\Column(type="integer")
    // */
    //private $id;

    /**
	 * @ORM\Id
     * @ORM\OneToOne(targetEntity=User::class, cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\Column(type="date")
     */
    private $dateDemande;

    public function __construct() {
    	$this->dateDemande = new DateTime();
	}

	//public function getId(): ?int
    //{
    //    return $this->id;
    //}

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getDateDemande(): ?\DateTimeInterface
    {
        return $this->dateDemande;
    }

    public function setDateDemande(\DateTimeInterface $dateDemande): self
    {
        $this->dateDemande = $dateDemande;

        return $this;
    }
}
