<?php

namespace App\Entity;

use App\Repository\DansCorbeilleRepository;
use DateInterval;
use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DansCorbeilleRepository::class)
 * @ORM\Table(name="corbeille")
 */
class PhotoDansCorbeille
{
    ///**
    // *
    // * @ORM\GeneratedValue
    // * @ORM\Column(type="integer")
    // */
    //private $id;

    /**
	 * @ORM\Id
     * @ORM\OneToOne(targetEntity=Photo::class, inversedBy="corbeille", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $photo;

    /**
     * @ORM\Column(type="date")
     */
    private $date_corbeille;


    public function __construct() {
    	$this->date_corbeille = new \DateTimeImmutable();
	}

	//public function getId(): ?int
    //{
    //    return $this->id;
    //}

    public function getPhoto(): ?Photo
    {
        return $this->photo;
    }

    public function setPhoto(?Photo $photo): self
    {
        $this->photo = $photo;

        return $this;
    }

    public function getDateCorbeille(): ?\DateTimeInterface
    {
        return $this->date_corbeille;
    }

    public function getDateSuppression(): ?\DateTimeInterface{
    	return (clone $this->getDateCorbeille())->add(new DateInterval('P30D'));
	}

	public function getTempsAvantSuppression(){
    	$diff = (new DateTime())->setTime(0,0)->diff($this->getDateSuppression());
    	return $diff->invert ? -$diff->days : $diff->days;
	}

    public function setDateCorbeille(\DateTimeInterface $date_corbeille): self
    {
        $this->date_corbeille = $date_corbeille;

        return $this;
    }
}
