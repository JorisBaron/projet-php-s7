<?php

namespace App\Entity;

use App\Repository\ReportedPhotoRepository;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ReportedPhotoRepository::class)
 */
class ReportedPhoto
{
    /**
     //*@ORM\Id
     //* @ORM\GeneratedValue
     //* @ORM\Column(type="integer")
     */
    //private $id;

    /**
	 * @ORM\Id
     * @ORM\OneToOne(targetEntity=Photo::class, inversedBy="reportedPhoto", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $photo;

    /**
     * @ORM\Column(type="integer", options={"default":0})
     */
    private $statut;

    /**
     * @ORM\OneToMany(targetEntity=Report::class, mappedBy="reportedPhoto", orphanRemoval=true)
     */
    private $reports;


    public function __construct() {
    	$this->reports = new ArrayCollection();
    	$this->statut = 0;
	}

	public function getId(): ?int
	{
	  return $this->getPhoto()->getId();
	}

    public function getPhoto(): ?Photo
    {
        return $this->photo;
    }

    public function setPhoto(Photo $photo): self
    {
        $this->photo = $photo;

        return $this;
    }

    public function getStatut(): ?int
    {
        return $this->statut;
    }

    public function setStatut(int $statut): self
    {
        $this->statut = $statut;

        return $this;
    }

	/**
	 * @param int $change
	 * @return $this
	 *
	 * @author Joris Baron
	 */
	public function updateStatut(int $change): self
	{
		$this->statut = max($this->statut + $change, 0);

		return $this;
	}

    /**
     * @return Collection|Report[]
     */
    public function getReports(): Collection
    {
        return $this->reports;
    }

    public function addReport(Report $report): self
    {
        if (!$this->reports->contains($report)) {
            $this->reports[] = $report;
            $report->setReportedPhoto($this);
        }

        return $this;
    }

    public function removeReport(Report $report): self
    {
        if ($this->reports->removeElement($report)) {
            // set the owning side to null (unless already changed)
            if ($report->getReportedPhoto() === $this) {
                $report->setReportedPhoto(null);
            }
        }

        return $this;
    }
}
