<?php

namespace App\Entity;

use App\Repository\AlbumRepository;
use DateTime;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AlbumRepository::class)
 */
class Album
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="albums")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $titre;

    /**
     * @ORM\Column(type="date")
     */
    private $date_creation;

    /**
     * @ORM\OneToMany(targetEntity=Photo::class, mappedBy="album")
     */
    private $photos;


    public function __construct() {
    	$this->date_creation = new DateTime();
     	$this->photos = new ArrayCollection();
	}

	public function getId(): ?int
	{
		return $this->id;
	}

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    public function getDateCreation(): ?DateTimeInterface
    {
        return $this->date_creation;
    }

    public function setDateCreation(DateTimeInterface $date_creation): self
    {
        $this->date_creation = $date_creation;

        return $this;
    }

    /**
     * @return Collection|Photo[]
     */
    public function getPhotos(): Collection
    {
        $photos_visibles = new ArrayCollection();
        foreach($this->photos as $photo) {
            if(!$photo->getCorbeille())
                $photos_visibles[] = $photo;
        }
        return $photos_visibles;
    }

    public function addPhoto(Photo $photo): self
    {
        if (!$this->photos->contains($photo)) {
            $this->photos[] = $photo;
            $photo->setAlbum($this);
        }

        return $this;
    }

    public function getP1Photo()
    {
        return $this->getPhotos()->get(0);
    }

    public function getP2Photo()
    {
        return $this->getPhotos()->get(1);
    }

    public function getP3Photo()
    {
        return $this->getPhotos()->get(2);
    }

    public function removePhoto(Photo $photo): self
    {
        if ($this->photos->contains($photo)) {
            $this->photos->removeElement($photo);
            // set the owning side to null (unless already changed)
            if ($photo->getAlbum() === $this) {
                $photo->setAlbum(null);
            }
        }

        return $this;
    }
}
