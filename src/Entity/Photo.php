<?php

namespace App\Entity;

use App\Repository\PhotoRepository;
use DateTime;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PhotoRepository::class)
 */
class Photo
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="photos")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity=Album::class, inversedBy="photos")
     */
    private $album;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $fichier;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $titre;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="datetime")
     */
    private $datePublication;

    /**
     * @ORM\OneToMany(targetEntity=Tag::class, mappedBy="photo", orphanRemoval=true)
     */
    private $tags;

    /**
     * @ORM\OneToOne(targetEntity=PhotoDansCorbeille::class, mappedBy="photo", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $corbeille;

	/**
	 * @ORM\OneToOne(targetEntity=ReportedPhoto::class, mappedBy="photo", cascade={"persist", "remove"}, orphanRemoval=true)
	 */
	private $reportedPhoto;

    public function __construct() {
    	$this->datePublication = new DateTime();
    	$this->tags = new ArrayCollection();
	}

	public function getId(): ?int
	{
		return $this->id;
	}

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getAlbum(): ?Album
    {
        return $this->album;
    }

    public function setAlbum(?Album $album): self
    {
        $this->album = $album;

        return $this;
    }

    public function getFichier(): ?string
    {
        return $this->fichier;
    }

    public function setFichier(string $fichier): self
    {
        $this->fichier = $fichier;

        return $this;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getDatePublication(): ?DateTimeInterface
    {
        return $this->datePublication;
    }

    public function setDatePublication(DateTimeInterface $datePublication): self
    {
        $this->datePublication = $datePublication;

        return $this;
    }

    /**
     * @return Collection|Tag[]
     */
    public function getTags(): Collection
    {
        return $this->tags;
    }

    public function addTag(Tag $tag): self
    {
        if (!$this->tags->contains($tag)) {
            $this->tags[] = $tag;
            $tag->setPhoto($this);
        }
        return $this;
    }

    public function addTagsS(array $tags): self
    {
        foreach($tags as $tag) {
            $this->addTag($tag);
        }
        return $this;
    }

    public function removeTag(Tag $tag): self
    {
        if ($this->tags->contains($tag)) {
            $this->tags->removeElement($tag);
            // set the owning side to null (unless already changed)
            if ($tag->getPhoto() === $this) {
                $tag->setPhoto(null);
            }
        }
        return $this;
    }

    public function removeTags(array $tags): self
    {
        foreach($tags as $tag) {
            $this->removeTag($tag);
        }
        return $this;
    }

    public function getCorbeille(): ?PhotoDansCorbeille
    {
        return $this->corbeille;
    }

    public function setCorbeille(?PhotoDansCorbeille $corbeille): self
    {
        $this->corbeille = $corbeille;

        // set the owning side of the relation if necessary
        if ($corbeille !== null && $corbeille->getPhoto() !== $this) {
            $corbeille->setPhoto($this);
        }

        return $this;
    }

	/**
	 * @return $this
	 *
	 * @author Joris Baron
	 */
    public function throwAway(): self {
    	if($this->getCorbeille() === null){
			$this->setCorbeille(new PhotoDansCorbeille());
		}
    	return $this;
	}

	/**
	 * @return $this
	 *
	 * @author Joris Baron
	 */
	public function restore(): self{
      		return $this->setCorbeille(null);
      	}

    public function getReportedPhoto(): ?ReportedPhoto
    {
        return $this->reportedPhoto;
    }

    public function setReportedPhoto(?ReportedPhoto $reportedPhoto): self
    {
        // unset the owning side of the relation if necessary
        if ($reportedPhoto === null && $this->reportedPhoto !== null) {
            $this->reportedPhoto->setPhoto(null);
        }

        // set the owning side of the relation if necessary
        if ($reportedPhoto !== null && $reportedPhoto->getPhoto() !== $this) {
            $reportedPhoto->setPhoto($this);
        }

        $this->reportedPhoto = $reportedPhoto;

        return $this;
    }
}
