<?php

namespace App\Entity;

use App\Repository\ReportRepository;
use DateTime;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ReportRepository::class)
 * @ORM\Table(uniqueConstraints={
 *     @ORM\UniqueConstraint(columns={"reported_photo_id", "user_id"})
 * })
 */
class Report
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=ReportedPhoto::class, inversedBy="reports")
     * @ORM\JoinColumn(nullable=false, referencedColumnName="photo_id")
     */
    private $reportedPhoto;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="reports")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateReport;

    public function __construct() {
    	$this->dateReport = new DateTime();
	}

	public function getId(): ?int
    {
        return $this->id;
    }

    public function getReportedPhoto(): ?ReportedPhoto
    {
        return $this->reportedPhoto;
    }

    public function setReportedPhoto(?ReportedPhoto $reportedPhoto): self
    {
        $this->reportedPhoto = $reportedPhoto;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getDateReport(): ?DateTimeInterface
    {
        return $this->dateReport;
    }

    public function setDateReport(DateTimeInterface $dateReport): self
    {
        $this->dateReport = $dateReport;

        return $this;
    }
}
