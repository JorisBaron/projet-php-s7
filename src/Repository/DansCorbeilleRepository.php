<?php

namespace App\Repository;

use App\Entity\PhotoDansCorbeille;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PhotoDansCorbeille|null find($id, $lockMode = null, $lockVersion = null)
 * @method PhotoDansCorbeille|null findOneBy(array $criteria, array $orderBy = null)
 * @method PhotoDansCorbeille[]    findAll()
 * @method PhotoDansCorbeille[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DansCorbeilleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PhotoDansCorbeille::class);
    }

    // /**
    //  * @return DansCorbeille[] Returns an array of DansCorbeille objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DansCorbeille
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
