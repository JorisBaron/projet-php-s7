<?php

namespace App\Repository;

use App\Entity\UserToDelete;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UserToDelete|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserToDelete|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserToDelete[]    findAll()
 * @method UserToDelete[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserToDeleteRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserToDelete::class);
    }

    // /**
    //  * @return UserToDelete[] Returns an array of UserToDelete objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UserToDelete
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
