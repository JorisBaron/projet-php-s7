<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 *
 * @method User|null findOneByEmail(string $email)
 * @method User[] findByEmail(string $email)
 * @method User|null findOneByPseudo(string $pseudo)
 * @method User[] findByPseudo(string $pseudo)
 * @method User|null findOneByPhoto(string $photo)
 * @method User[] findByPhoto(string $photo)
 */
class UserRepository extends ServiceEntityRepository implements PasswordUpgraderInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

	/**
	 * Used to upgrade (rehash) the user's password automatically over time.
	 * @param UserInterface $user
	 * @param string $newEncodedPassword
	 * @throws \Doctrine\ORM\ORMException
	 * @throws \Doctrine\ORM\OptimisticLockException
	 */
    public function upgradePassword(UserInterface $user, string $newEncodedPassword): void
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', \get_class($user)));
        }

        $user->setPassword($newEncodedPassword);
        $this->_em->persist($user);
        $this->_em->flush();
    }

	/**
	 * @param string $pseudo
	 * @return User[]
	 *
	 * @author Joris Baron
	 * @author Elhadj Diallo
	 */
    public function findByPseudoStartingWith(string $pseudo): array {
    	$qb = $this->createQueryBuilder('u')
			->where("u.pseudo LIKE CONCAT(:pseudo, '%')")
			->setParameter('pseudo', $pseudo);
    	return $qb->getQuery()->execute();
	}

	/**
	 * @param string $nomPrenom
	 * @return User[]
	 *
	 * @author Joris Baron
	 * @author Elhadj Diallo
	 */
	public function findByNomPrenomStartingWith(string $nomPrenom): array {
		$qb = $this->createQueryBuilder('u')
			->where("TRIM(CONCAT(CASE WHEN u.nom IS NULL THEN '' ELSE u.nom END, ' ', CASE WHEN u.prenom IS NULL THEN '' ELSE u.prenom END)) LIKE CONCAT(:np, '%')")
			->orWhere("TRIM(CONCAT(CASE WHEN u.prenom IS NULL THEN '' ELSE u.prenom END, ' ', CASE WHEN u.nom IS NULL THEN '' ELSE u.nom END)) LIKE CONCAT(:np, '%')")
			->setParameter('np', $nomPrenom);
		return $qb->getQuery()->execute();
	}

    // /**
    //  * @return User[] Returns an array of User objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?User
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
