<?php

namespace App\Repository;

use App\Entity\Photo;
use App\Entity\ReportedPhoto;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ReportedPhoto|null find($id, $lockMode = null, $lockVersion = null)
 * @method ReportedPhoto|null findOneBy(array $criteria, array $orderBy = null)
 * @method ReportedPhoto[]    findAll()
 * @method ReportedPhoto[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 * @method ReportedPhoto|null findOneByPhoto(Photo $photo, array $orderBy = null)
 * @method ReportedPhoto[]    findByPhoto(Photo $photo, array $orderBy = null, $limit = null, $offset = null)
 */
class ReportedPhotoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, ReportedPhoto::class);
    }

	/**
	 * @param array|null $orderBy
	 * @param null $limit
	 * @return array
	 */
    public function findAllOrderedLimited($orderBy = 'asc', $limit = null): array {
		return $this->createQueryBuilder('r')
			->orderBy('r.id',$orderBy)
			->setMaxResults($limit)
			->getQuery()
			->getResult();
	}

    // /**
    //  * @return ReportedPhoto[] Returns an array of ReportedPhoto objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ReportedPhoto
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
