<?php

namespace App\Repository;

use App\Entity\Photo;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Photo|null find($id, $lockMode = null, $lockVersion = null)
 * @method Photo|null findOneBy(array $criteria, array $orderBy = null)
 * @method Photo[]    findAll()
 * @method Photo[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 *
 * @method Photo|null findOneByUser(User $user, array $orderBy = null)
 * @method Photo[]    findByUser(User $user, array $orderBy = null, $limit = null, $offset = null)
 * @method Photo|null findOneByFichier(string $fichier, array $orderBy = null)
 * @method Photo[]    findByFichier(string $fichier, array $orderBy = null, $limit = null, $offset = null)
 */
class PhotoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Photo::class);
    }

    /**
     * @param string|null $term
     * @return QueryBuilder
     */
    public function getWithSearchQueryBuilder(?string $term): QueryBuilder
    {
        $qb = $this->createQueryBuilder('photo_request');
        return $qb
            ->orderBy('date_publication', 'DESC');
    }

    public function getPage(Photo $photo){
		$nbPhotos = $this->createQueryBuilder('p')
			->select('count(p.id)')
			->where('p.datePublication >= :d')
			->andWhere('p.user = :u')
			->setParameter('d', $photo->getDatePublication())
			->setParameter('u', $photo->getUser()->getId())
			->getQuery()->getSingleScalarResult();

		return (int)(($nbPhotos-1)/9)+1;
	}

	/**
	 * @param array $criteria
	 * @param array|null $orderBy
	 * @param null $limit
	 * @param null $offset
	 * @return Photo[]
	 */
	public function findDisplayableBy(array $criteria, array $orderBy = null, $limit = null, $offset = null): array {
    	$p = $this->findBy($criteria, $orderBy, $limit, $offset);
    	return array_filter($p, function (Photo $photo){
    		return $photo->getCorbeille() === null;
		});
	}

//	public function findByCriteria(array $criteria, array $orderBy = null) {
//        $qb = $this->createQueryBuilder('photo');
//
//        $qb->select('photo')
//            ->from('App:Photo', 'photo');
//
//            //album
//        if(!empty($criteria['albums'])) {
//            $qb->where(
//                $qb->expr()->in('photo.album', $criteria['albums'])
//            );
//        }
//        // tags
//        if(!empty($criteria['tags'])) {
//            $qb->andWhere(
//                $qb->expr()->in('photo.album', $criteria['albums'])
//            )
//            // date
//            ->where('photo.datePublication >= :dateDebut')
//            ->andWhere('photo.datePublication <= :dateFin')
//            ->setParameters([
//                'dateDebut'=> $criteria['dateDebut'],
//                'dateFin' => $criteria['dateFin']
//            ]);
//
//	    $photos = $qb->getQuery()->getResult();
//	    return $photos;
//    }

    // /**
    //  * @return Photo[] Returns an array of Photo objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Photo
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
