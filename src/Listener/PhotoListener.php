<?php


namespace App\Listener;


use App\Entity\Photo;
use Doctrine\ORM\Mapping\PreRemove;
use Doctrine\Persistence\Event\LifecycleEventArgs;

class PhotoListener {
	private $photoDirectory;

	public function __construct($photoDirectory) {
		$this->photoDirectory = $photoDirectory;
	}

	/** @PreRemove
	 * @param Photo $photo
	 * @param LifecycleEventArgs $event
	 */
	public function preRemove(Photo $photo, LifecycleEventArgs $event) {
		if($photo->getFichier() !== null){
			unlink($this->photoDirectory . DIRECTORY_SEPARATOR . $photo->getFichier());
		}
	}
}