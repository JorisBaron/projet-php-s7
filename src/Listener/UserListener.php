<?php


namespace App\Listener;


use App\Entity\Photo;
use App\Entity\User;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Mapping as ORM;

class UserListener {
	private $ppDirectory;

	public function __construct($ppDirectory) {
		$this->ppDirectory = $ppDirectory;
	}

	/** @ORM\PreUpdate
	 * @param User $user
	 * @param PreUpdateEventArgs $event
	 */
	public function preUpdate(User $user, PreUpdateEventArgs $event) {
		if($event->hasChangedField('photo') && $event->getOldValue('photo') !== null){
			unlink($this->ppDirectory . DIRECTORY_SEPARATOR . $event->getOldValue('photo'));
		}
	}
}