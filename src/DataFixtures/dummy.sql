SET FOREIGN_KEY_CHECKS = 0;
truncate table admin;
truncate table corbeille;
truncate table relation;
truncate table report;
truncate table reported_photo;
truncate table reset_password_request;
truncate table tag;
truncate table photo;
truncate table album;
truncate table user_to_delete;
truncate table `user`;
truncate table generic_user;
SET FOREIGN_KEY_CHECKS = 1;

INSERT INTO `generic_user` (`id`, `email`, `roles`, `password`, `nom`, `prenom`, `type`)
VALUES (8, 'jooj@jooj.io', '[\"ROLE_USER\"]', '$argon2id$v=19$m=65536,t=4,p=1$MXZEbmFFLmdzZEdyZ2NiMw$+jKom3Y4Rkx26FRM4YzY0DkeJulZijETYTPI2r5Q1HA', NULL, NULL, 'user'),
       (9, 'joris.baron@yahoo.fr', '[\"ROLE_USER\"]', '$argon2id$v=19$m=65536,t=4,p=1$MXZEbmFFLmdzZEdyZ2NiMw$+jKom3Y4Rkx26FRM4YzY0DkeJulZijETYTPI2r5Q1HA', NULL, NULL, 'user'),
       (10, 'mauraneglaude@gmail.com', '[\"ROLE_USER\"]', '$argon2id$v=19$m=65536,t=4,p=1$MTh5MUNTT2VEUjB5dGlSYw$5p+a5DZMZeqtI7kdtTT6eNoSvbMtv2coqRicLIXn2iM', NULL, NULL, 'user'),
       (11, 'stalker@stalker.com', '[\"ROLE_USER\"]', '$argon2id$v=19$m=65536,t=4,p=1$ZlNjQnNDQno1bDFlNnRLcQ$I09gbUEY0WGE3kA0X+qIEiCH/2PMi2F5EqFk/+YqZmA', 'Thatonne', 'Vomas', 'user'),
       (12, 'zami@alan.com', '[\"ROLE_USER\"]', '$argon2id$v=19$m=65536,t=4,p=1$TnNtbHhlWm9ZbjNaZDdmYg$Ko1DVQYRkmZF0TMZohbgOgltOBDX2DPO+l+khV+cHsg', 'Alan', 'Zami', 'user'),
       (13, 'glaurane@maude.com', '[\"ROLE_USER\"]', '$argon2id$v=19$m=65536,t=4,p=1$MTh5MUNTT2VEUjB5dGlSYw$5p+a5DZMZeqtI7kdtTT6eNoSvbMtv2coqRicLIXn2iM', 'Maude', 'Glaurane', 'user'),
       (14,'ouais-b@jooj.io','[\"ROLE_ADMIN\"]','$argon2id$v=19$m=65536,t=4,p=1$MXZEbmFFLmdzZEdyZ2NiMw$+jKom3Y4Rkx26FRM4YzY0DkeJulZijETYTPI2r5Q1HA',NULL,NULL,'admin');



INSERT INTO `user` (`id`,  `pseudo`, `photo`, `date_naissance`, `date_inscription`, `public`) VALUES
(8, 'JooJ', NULL, '1998-01-01', '2020-10-17', 0),
(9,  'JBaron', NULL, '1998-01-01', '2020-10-17', 0),
(10, 'nuna', NULL, '1996-02-22', '2020-10-17', 0),
(11, 'thatonneDu06', '5fac8f4e116b22.917132155fac8f4e116ba7.70706815.jpeg', '1999-01-01', '2020-11-12', 0),
(12, 'bestouOstyle', '5fac91a684abe3.637910575fac91a684aca2.53165165.jpeg', '1999-01-01', '2020-11-12', 1),
(13, 'philactere', '5fad092c27e8e7.551552855fad092c27e980.67381218.jpeg', '1999-01-01', '2020-11-12', 0);

INSERT INTO `admin`
VALUES (14);


INSERT INTO `relation` (`id`, `user1_id`, `user2_id`, `statut`) VALUES
(3, 8, 9, 0),
(5, 8, 10, 1),
(7, 13, 12, 1);

INSERT INTO `album` (`id`, `user_id`, `titre`, `date_creation`) VALUES
(1, 10, 'the album', '2020-10-24'),
(9, 10, 'test_album_afficher', '2020-11-15'),
(33, 8, 'Favoris', '2020-12-14'),
(46, 8, 'J\'ai faim', '2020-12-15'),
(47, 8, 'un nouvel album', '2020-12-16'),
(56, 8, 'a', '2020-12-19');

INSERT INTO `photo` (`id`, `user_id`, `album_id`, `fichier`, `titre`, `description`, `date_publication`) VALUES
(19, 10, NULL, 'amelie-poulain-5f95a6a495cea.jpeg', 'fdqsfdsq', 'fdsqfds', '2020-10-25'),
(53, 10, NULL, 'camila-damasio-mWYhrOiAgmA-unsplash-5fa7f77c03b5a.jpeg', 'Mon super confinement', 'trop cooool', '2020-11-08'),
(54, 10, NULL, 'dom-hill-nimElTcTNyY-unsplash-5fa7f82a28f8a.jpeg', 'moi jeune !!', 'hihihi\r\nOOTD : street style jaune pour donner du pep\'s à ma vie et mon entourage !!', '2020-11-08'),
(55, 10, NULL, 'sincerely-media-nGrfKmtwv24-unsplash-5fa7fb2d5297c.jpeg', 'test', 'test', '2020-11-08'),
(56, 10, NULL, 'joseph-gonzalez-zcUgjyqEwe8-unsplash-5fa7fe0d73705.jpeg', 'bigggg', NULL, '2020-11-08'),
(57, 10, NULL, 'tyrell-james-GwmtL6alcTk-unsplash-5fa7fe2d337c9.jpeg', 'ah ouaiiis', NULL, '2020-11-08'),
(58, 10, NULL, 'toa-heftiba-QRFXkkvaD7Q-unsplash-5fa7fe4247b82.jpeg', 'normaaal pour la pagintation', NULL, '2020-11-08'),
(59, 10, NULL, 'chewy-ScEXB6uxOVc-unsplash-5fa7fe5367f7e.jpeg', 'Mon chien et moi vs le monde', 'On fait une petite description là', '2020-11-08'),
(60, 10, NULL, 'sanni-sahil-LEaK1Lmd1a8-unsplash-5fa8086c821a6.jpeg', 'Nos photos', 'On a revu et fini l\'affichage des photos, qu\'en pensez-vous ? ;o', '2020-11-08'),
(64, 10, NULL, 'tuyen-vo-sporj1nu574-unsplash-5fa81f2cf4056.jpeg', 'test tag length', 'test de la largeur', '2020-11-08'),
(66, 10, 1, 'Annotation-2020-03-19-173530-5fa86746ad49c.png', 'Pp d\'un gars que j\'aime pas', 'Ceci est une pp d\'un gars que je n\'apprécie, mais alors, vraiment pas. Je l\'ai prise à la volée pour des recherches Google qui n\'ont en plus pas été fructueuses. En revanche, depuis cet instant, je me suis rendue compte que c\'est intéressant de conserver une trace des photos de profil des utilisateurs que je croise. ;)', '2020-11-08'),
(67, 10, NULL, 'best-putin-5fa960b56ecc6.png', 'test_changement', 'je change ça', '2020-11-09'),
(68, 10, NULL, 'clouds-731260-640-5fa9b188bae6f.jpeg', 'ajout_photo_9', 'une description', '2020-11-09'),
(74, 8, 56, 'hwasa-5fab293d6906a.jpeg', 'hwa sa', 'Ceci est ma femme. Elle est tout ce dont j\'ai rêvé ... love you, Hwa sa', '2020-11-10'),
(75, 10, NULL, 'b1b3323b8ca8328aa662176f26941c6c-5fab353966a5d.jpeg', 'mon meme', 'trop drole mon meme j\'adore', '2020-11-11'),
(76, 12, NULL, 'savannah-class-8E-sDeSqYi0-unsplash-5fac922a5122a.jpeg', 'Ma première photo', NULL, '2020-11-02'),
(77, 12, NULL, 'tyrell-james-GwmtL6alcTk-unsplash-5fac923a60186.jpeg', 'Shooting #1', 'J\'ai eu la chance de dessiner et confectionner cet outfit pour un shooting. Ma carrière ne fait que commencer.', '2020-11-02'),
(78, 12, NULL, 'bahador-c4Gkeluuj8s-unsplash-5fac928e344ad.jpeg', 'Ma troisieme photo', NULL, '2020-11-02'),
(80, 12, NULL, 'chermiti-mohamed-2-vx1puVPIM-unsplash-5fac9299cf272.jpeg', 'Rousseur', 'Souriez !', '2020-11-02'),
(81, 12, NULL, 'jason-tran-o2DpOL6zLI-unsplash-5fac92a84dad0.jpeg', 'Ma cinquième photo', NULL, '2020-11-02'),
(82, 12, NULL, 'giorgio-trovato-U-DinasrjvU-unsplash-5fac92d174388.jpeg', 'Vie de styliste ... 2', 'Cette photo représente exactement ma vie de styliste. \r\nJe l\'adore !!!', '2020-11-11'),
(83, 12, NULL, 'khaled-ghareeb-XRDvrd5N5Ww-unsplash-5fac934134328.jpeg', 'Défilé #1', 'Quelle joie de retrouver mon oeuvre portée par un modèle de qualité ...\r\nJ\'ai dormi sur mes deux oreilles cette nuit-là 😊', '2020-11-10'),
(84, 12, NULL, 'karsten-winegeart-7vhqnO-sT88-unsplash-5fac941120310.jpeg', 'Orange orange orange', 'J\'adore le orange, j\'adore les chiens.', '2020-11-11'),
(85, 12, NULL, 'jessica-felicio-QS9ZX5UnS14-unsplash-5fac9462b26a4.jpeg', 'Profil', 'J\'ai trouvé cette photo de profil de manière totalement aléatoire ...\r\nElle est vraiment bluffante ...\r\nHâte de mettre l\'inspiration qui m\'est venu dans ma prochaine création 👀', '2020-11-11'),
(86, 12, NULL, 'lauren-fleischmann-R2aodqJn3b8-unsplash-5fac94d56003c.jpeg', 'Vie de Style 1', '1 : Toujours avoir plusieurs basiques de différentes couleurs !!!!\r\n2 : Les couleurs des basiques doivent avoir un ton neutre !!!!\r\nTous ceux et celles qui ne suivent pas ces règles sont déclarés out of fashion', '2020-11-12'),
(87, 12, NULL, 't11-5fac962ad1382.jpeg', 'T11', '~SmileToLife~\r\nJe souris à la vie ...', '2020-11-12'),
(89, 13, NULL, 'sanni-sahil-LEaK1Lmd1a8-unsplash-5fad0a32880e8.jpeg', 'Ma première vraie photo', 'Cette photo avait besoin d\'une description', '2020-11-12'),
(90, 13, NULL, 'chermiti-mohamed-2-vx1puVPIM-unsplash-5fad96c21cc0f.jpeg', 'bonjour', 'production', '2020-11-12'),
(91, 13, NULL, 'jessica-felicio-QS9ZX5UnS14-unsplash-5fad99fe6d78b.jpeg', 'test', 'dsqfdsqf', '2020-11-12'),
(92, 13, NULL, 'giorgio-trovato-U-DinasrjvU-unsplash-5fad9a28d6a2a.jpeg', 'hiii', 'testtt', '2020-11-12'),
(93, 8, 33, 'yodaa-5fcfe4eba1e64.png', 'testeq', NULL, '2020-12-08'),
(94, 8, 33, 'Affiche-d-art-Cinema-Jules-par-Joshua-Budich-5fd263c77e2cc.jpeg', 'test', NULL, '2020-12-10'),
(95, 8, 33, '5fd8da4f7946f4.224752645fd8da4f794747.25625412.jpeg', 'Nouvelle photo', 'GNEEEE', '2020-12-15'),
(97, 8, NULL, '5fdde957efc2c6.067479675fdde957efc315.58047335.jpeg', 'lol', NULL, '2020-12-19'),
(98, 8, NULL, '5fdde9742f08c7.744866455fdde9742f08f1.85921551.jpeg', 'héhé', NULL, '2020-12-19'),
(99, 8, NULL, '5fdde98173fe67.017914475fdde98173fea4.41614951.png', 'bebe udp', NULL, '2020-12-19'),
(100, 8, NULL, '5fdde98a550ae6.784522955fdde98a550b29.43879306.jpeg', 'princesse', NULL, '2020-12-19'),
(101, 8, NULL, '5fdde99c8deb11.530003285fdde99c8deb58.61892853.png', 'hahahaha', NULL, '2020-12-19'),
(102, 8, NULL, '5fdde9a7c85622.573086575fdde9a7c85665.82373734.jpeg', 'hihihi', NULL, '2020-12-19');

INSERT INTO `corbeille` (`photo_id`, `date_corbeille`) VALUES
(74, '2020-12-19');


INSERT INTO `tag` (`id`, `photo_id`, `text`) VALUES
(5, 53, 'trop drole'),
(6, 54, 'ootd'),
(7, 54, 'flawless'),
(8, 54, 'selfless'),
(9, 54, 'noraj'),
(10, 55, 'test'),
(11, 60, 'jooj'),
(12, 60, 'yero'),
(17, 64, 'jdqslmkfdjslqmf'),
(25, 59, 'dogs'),
(26, 59, 'bestfriend'),
(27, 59, 'bff'),
(28, 59, 'human<dogs'),
(29, 66, 'stalking'),
(30, 66, 'lol'),
(31, 66, 'blithou'),
(32, 66, 'vieuxmec'),
(33, 66, 'norajdemondepecage'),
(34, 66, 'googleimage'),
(53, 67, 'testtt'),
(55, 67, 'test_dajout_de_tag'),
(58, 67, 'coucou je test'),
(65, 74, 'hwasa'),
(66, 75, 'hahaha'),
(67, 82, 'enjoylife'),
(68, 82, 'stylistlife'),
(69, 82, 'stylist'),
(70, 82, 'love'),
(71, 82, 'photo'),
(72, 83, 'joie'),
(73, 84, 'orange'),
(74, 84, 'dog'),
(75, 84, 'life'),
(76, 85, 'creation'),
(77, 85, 'inspiration'),
(78, 85, 'bluff'),
(79, 86, 'outoffashion'),
(80, 80, 'sourire'),
(81, 80, 'ginger'),
(82, 77, 'carrier'),
(83, 77, 'life'),
(84, 77, 'lifestyle'),
(85, 77, 'enjoylife'),
(86, 77, 't11'),
(87, 87, 'smiletolife'),
(88, 87, 't11'),
(89, 87, 'kms'),
(90, 87, 'depressedbutsmilling'),
(91, 87, 'everythingisfine'),
(92, 89, 'description'),
(93, 89, 'test d\'un tag'),
(94, 89, 'fdsqfd'),
(95, 89, 'fdsqfdsq'),
(97, 91, 'fdujksqlmfd'),
(98, 92, 'add'),
(99, 92, 'test'),
(100, 92, 'test'),
(101, 92, 'test'),
(102, 92, 'hahahaah'),
(103, 95, 'test'),
(104, 95, 'coucou'),
(105, 95, 'hahaha'),
(110, 74, 'love'),
(112, 74, 'mamamoo'),
(113, 74, 'twit');

INSERT INTO `reported_photo`
VALUES (100,0);

INSERT INTO `report`
VALUES (5,100,9,'2020-12-19 20:29:21'),
       (10,100,10,'2020-12-19 21:02:51');