<?php

namespace App\Form;

use App\Entity\Album;
use App\Entity\Photo;
use App\Repository\AlbumRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\NotBlank;

/** @author Maurane Glaude, maybe Joris Baron ? */
class PhotoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $user = $builder->getData()->getUser();

        // create the builder
        $builder
            ->add('titre', TextType::class, [
            	'label' => 'Titre',
            	'empty_data' => "",
            	'constraints' => [
            		new NotBlank([
            			'message' => "Le titre de la photo ne peut pas être vide"
					])
				]
			])
            ->add('fichierInput', FileType::class, [
                'data_class' => null,
				'mapped' => false,

				'label'=> 'Fichier',
				'help' => "2Mo maximum, formats autorisés : jpg/jpeg et png",
                'constraints' => [
					new NotBlank([
						'message' => "Vous devez ajouter un fichier"
					]),
                    new File([
                        'maxSize' => '5M',
                        'maxSizeMessage' => "L'image est trop grosse ({{ size }}{{ suffix }}o, le maximum est de {{ limit }}{{ suffix }}o)",
                        'mimeTypes' => [
                            'image/jpeg',
                            'image/png',
                        ],
                        'mimeTypesMessage' => "L'image n'est pas au bon format. Seuls les formats JPEG/JPG et PNG sont autorisés",
                    ])
                ],
            ])
            ->add('description', TextareaType::class, [
                'required' => false,
				'label' => 'Description'
            ])
            ->add('album', EntityType::class, [
            	'label' => 'Album',
                'class' => Album::class,
                'choice_label' => function($album) {
                	return $album->getTitre();
                },
                'query_builder' => function(AlbumRepository $ar) use($user) {
                    return $ar->createQueryBuilder('a')
                        ->where('a.user = :user')
                        ->setParameter('user', $user);
                },
                'required' => false,
            ])
            ->add('tags', CollectionType::class, [
                'entry_type' => TagType::class,
				'entry_options'  => [
					'compound'  => true,
				],
                'required' => false,
                'allow_add' => true,
                'allow_delete' => true,
                'prototype' => true,
                'by_reference' => false,
                'block_name' => 'tag_list',
            ]);
        $builder->add('submit', SubmitType::class,[
			'label'=>'Envoyer',
		]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Photo::class,
        ]);
    }
}
