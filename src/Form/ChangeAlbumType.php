<?php

namespace App\Form;

use App\Entity\Album;
use App\Entity\Photo;
use App\Repository\AlbumRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ChangeAlbumType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $user = $builder->getData()->getUser();

        $builder
            ->add('album', EntityType::class, [
                'label' => 'Album',
                'class' => Album::class,
                'choice_label' => function($album) {
                    return $album->getTitre();
                },
                'query_builder' => function(AlbumRepository $ar) use($user) {
                    return $ar->createQueryBuilder('a')
                        ->where('a.user = :user')
                        ->setParameter('user', $user);
                },
                'required' => false,
                'empty_data' => null,
                'placeholder' => '(Pas d\'album)'
            ])
            ->add('valider', SubmitType::class, [
                'attr' => ['class' => 'btn btn-primary'],
                'label' => 'Valider'
            ])
            ->add('annuler', ButtonType::class, [
                'attr' => ['class' => 'btn btn-secondary'],
                'label' => 'Finalement ...'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Photo::class,
        ]);
    }
}
