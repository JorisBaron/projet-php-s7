<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Image;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;

class EditProfileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
           
        ->add('pseudo', TextType::class, [
            'required' => true,
            'label' => 'Pseudo',
            'help' => 'Au moins 3 caractères, caractères autorisés : alphanumériques et <span class="text-nowrap">_ - .</span>',
            'help_html' => true,
            'constraints' => [
                new Length([
                    'min' => 3,
                    'minMessage' => 'Le pseudo contenir au moins {{ limit }} caractères',
                    'max' => 4096,
                ]),
                new Regex([
                    'pattern' => "#^[a-zA-Z0-9_.-]{3,}$#",
                    'message' => "Le pseudo contient des caractères invalides"
                ]),
            ],
        ]) 
            ->add('nom', TextType::class, [
				'required' => false,
				'label' => 'Nom',
			])
            ->add('prenom', TextType::class, [
				'required' => false,
				'label' => 'Prénom',
			])    
            ->add('email', EmailType::class, [
            	'required' => true,
				'label' => 'Email',
				'constraints' => [
					new NotBlank([
						'message' => "L'email ne peut pas être vide",
					]),
					new Email([
						'mode' => 'html5',
						'message' => 'Entrer un email valide'
					]),
				],
			])
            ->add('photoFile', FileType::class, [
				'required' => false,
            	'mapped' => false,
            	'label' => 'Photo de profil',
				'help' => "2Mo maximum, formats autorisés : jpg/jpeg et png",
				'constraints' => [
					new Image([
						'maxSize' => '2M',
						'maxSizeMessage' => "L'image est trop grosse ({{ size }}{{ suffix }}o, le maximum est de {{ limit }}{{ suffix }}o)",
						'mimeTypes' => ['image/jpeg','image/png'],
						'mimeTypesMessage' => "L'image n'est pas au bon format. Seuls les formats JPEG/JPG et PNG sont autorisés",
						'maxRatio' => 4,
						'maxRatioMessage' => "L'image est trop large",
						'minRatio' => 0.25,
						'minRatioMessage' => "L'image est trop haute",
					]),
				],
			])
            ->add('valider', SubmitType::class) ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
