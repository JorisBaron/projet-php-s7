<?php

namespace App\Form;

use App\Entity\Album;
use App\Entity\Tag;
use App\Repository\AlbumRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateIntervalType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class FilterType
 * @package App\Form
 * @author Maurane Glaude
 */
class FilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $user = $options['user'];
        $builder
            ->add('albums', EntityType::class, [
                'class' => Album::class,
                'query_builder' => function(AlbumRepository $ar) use($user) {
                    return $ar->createQueryBuilder('a')
                        ->where('a.user = :user')
                        ->setParameter('user', $user);
                },
                'choice_label' => function($album) {
                    return $album->getTitre();
                },
                'multiple' => true,
                'required' => false,
                'expanded' => true,
                'placeholder' => false,
            ])
            ->add('tags', ChoiceType::class, [
                'choices' => $options['tagTextList'],
                'multiple' => true,
                'required' => false,
                'expanded' => true,
                'placeholder' => false,
            ])
            ->add('date_debut', DateIntervalType::class, [
                'placeholder' => ['years' => '1970', 'months' => '01', 'days' => '01'],
                'required' => false,
                'widget' => 'integer'
            ])
            ->add('date_fin', DateIntervalType::class, [
                'placeholder' => ['years' => '1970', 'months' => '01', 'days' => '02'],
                'required' => false,
                'widget' => 'integer',
            ])
            ->add('appliquer', SubmitType::class, [
                'label' => 'Appliquer les filtres'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'tagTextList' => [],
            'user' => null,
        ]);
    }
}
