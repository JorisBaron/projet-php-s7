<?php

namespace App\Controller;

use App\Entity\ReportedPhoto;
use App\Repository\ReportedPhotoRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ReportsController extends AbstractController
{
	public function index(): Response
	{
		$em = $this->getDoctrine()->getManager();

		/** @var ReportedPhotoRepository $repoReport */
		$repoReport = $em->getRepository(ReportedPhoto::class);

		$reportedPhotos = $repoReport->findAllOrderedLimited('ASC',20);

		return $this->render('admin/reports.html.twig', [
			'reportedPhotos' => $reportedPhotos
		]);
	}

	public function getListJson(): Response {
		$em = $this->getDoctrine()->getManager();

		/** @var ReportedPhotoRepository $repoReport */
		$repoReport = $em->getRepository(ReportedPhoto::class);

		$reportedPhotos = $repoReport->findAllOrderedLimited('ASC',20);

		return $this->json(array_map(function (ReportedPhoto $reportedPhoto){
			return [
				'id' => $reportedPhoto->getId(),
				'statut' => $reportedPhoto->getStatut(),
				'count' => $reportedPhoto->getReports()->count(),
			];
		}, $reportedPhotos));
	}

	public function getListView(): Response {
		$em = $this->getDoctrine()->getManager();

		/** @var ReportedPhotoRepository $repoReport */
		$repoReport = $em->getRepository(ReportedPhoto::class);

		$reportedPhotos = $repoReport->findAllOrderedLimited('ASC',20);

		return $this->render('admin/report.list.html.twig', [
			'reportedPhotos' => $reportedPhotos
		]);
		$response = $this->render('admin/report.list.html.twig', [
			'reportedPhotos' => $reportedPhotos
		]);
		return new Response($response, 200);
	}

	public function getReportView(int $reportId): Response {
		$em = $this->getDoctrine()->getManager();

		/** @var ReportedPhotoRepository $repoReport */
		$repoReport = $em->getRepository(ReportedPhoto::class);

		$reportedPhoto = $repoReport->find($reportId);
		if($reportedPhoto === null){
			return new Response('Not Found', 404);
		}

		$response = $this->renderView("admin/report-view.html.twig", [
			"reportedPhoto" => $reportedPhoto,
		]);
		return new Response($response, 200);
	}

	public function addLock(int $reportId): Response {
		return $this->changeStatus($reportId, 1);
	}

	public function removeLock(int $reportId): Response {
		return $this->changeStatus($reportId, -1);
	}

	public function changeStatus(int $reportId, int $change): Response {
		$em = $this->getDoctrine()->getManager();

		/** @var ReportedPhotoRepository $repoReport */
		$repoReport = $em->getRepository(ReportedPhoto::class);

		$report = $repoReport->find($reportId);
		if($report === null){
			return new Response('Not Found', 404);
		}
		$report->updateStatut($change);
		$em->flush();

		return new Response(null, 204);
	}


	public function close(int $reportId): Response {
		$em = $this->getDoctrine()->getManager();

		/** @var ReportedPhotoRepository $repoReport */
		$repoReport = $em->getRepository(ReportedPhoto::class);

		$report = $repoReport->find($reportId);
		if($report === null){
			return new Response('Not Found', 404);
		}
		$em->remove($report);
		$em->flush();

		return new Response(null, 204);
	}
}
