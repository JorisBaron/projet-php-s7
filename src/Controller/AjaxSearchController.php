<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AjaxSearchController
 * @package App\Controller
 *
 * @author Joris Baron
 * @author Elhadj Diallo
 */
class AjaxSearchController extends AbstractController
{
	/**
	 * @param string $pseudo
	 * @return Response
	 */
    public function userByPseudo(string $pseudo): Response {
		$em = $this->getDoctrine()->getManager();

		/** @var UserRepository $repoUser */
		$repoUser = $em->getRepository(User::class);
		$users = $repoUser->findByPseudoStartingWith($pseudo);

        return $this->render('ajax_search/search-results.html.twig', [
            'users' => $users,
        ]);
    }

	public function userByNomPrenom(string $nomPrenom): Response {
		$em = $this->getDoctrine()->getManager();

		/** @var UserRepository $repoUser */
		$repoUser = $em->getRepository(User::class);
		$users = $repoUser->findByNomPrenomStartingWith($nomPrenom);

		return $this->render('ajax_search/search-results.html.twig', [
			'users' => $users,
		]);
	}

	/**
	 * @param string $search
	 * @return Response
	 */
	public function searchUser(string $search): Response {
		$em = $this->getDoctrine()->getManager();

		/** @var UserRepository $repoUser */
		$repoUser = $em->getRepository(User::class);
		$users = $repoUser->findByPseudoStartingWith($search);

		foreach($repoUser->findByNomPrenomStartingWith($search) as $u){
			if(!in_array($u, $users, true)){
				$users[] = $u;
			}
		}

		usort($users, function (User $u1, User $u2){
			return strcasecmp($u1->getPseudo(), $u2->getPseudo());
		});


		return $this->render('ajax_search/search-results.html.twig', [
			'users' => $users,
		]);
	}
}
