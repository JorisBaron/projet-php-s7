<?php

namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CommonController extends AbstractController {

    /**
     * @Route("/not_found/", name="not_found")
     */
    function not_found() {
        $html = $this->render("@Twig/Exception/error404.html.twig");
        return new Response($html, 404, [
            'Content-type' => 'text/html'
        ]);
    }
}
