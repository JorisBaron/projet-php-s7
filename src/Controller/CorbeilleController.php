<?php

namespace App\Controller;

use App\Entity\Photo;
use App\Entity\PhotoDansCorbeille;
use App\Entity\User;
use App\Repository\DansCorbeilleRepository;
use App\Repository\PhotoRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CorbeilleController extends AbstractController
{
    public function index(): Response
    {
		$em = $this->getDoctrine()->getManager();

		/** @var DansCorbeilleRepository $repoUser */
		//$repoCorbeille = $em->getRepository(PhotoDansCorbeille::class);
		/** @var PhotoRepository $repoUser */
		//$repoPhoto = $em->getRepository(Photo::class);

		/** @var User $user */
		$user = $this->getUser();

		$photosInCorbeille = $user->getPhotosInCorbeille();
		/** @var Photo $photo */
		foreach($photosInCorbeille as $photo){
			if($photo->getCorbeille()->getTempsAvantSuppression() <= 0 ){
				$em->remove($photo);
				$photosInCorbeille->removeElement($photo);
			}
		}

		$em->flush();

        return $this->render('corbeille/index.html.twig', [
			'photos' => $photosInCorbeille,
        ]);
    }

    public function getPhoto(int $photo): Response {
		/** @var User $user */
		$user = $this->getUser();

		if($user === null){
			return new Response("Unauthorized", 401);
		}

		$em = $this->getDoctrine();

		/** @var Photo $photo retrieving asked photo */
		$photo = $em->getRepository(Photo::class)->find($photo);
		if($photo === null){
			return new Response(null, 404);
		}

		$photoUser = $photo->getUser();
		// vérifie le droit d'accès
		if($user !== $photoUser) {
			return new Response(null, 403);
		}

		return $this->render("corbeille/includes/detail.photo.html.twig", [
			"photo" => $photo
		]);

	}
}
