<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ListeAmisController
 * @package App\Controller
 *
 * @author Joris Baron
 */
class ListeAmisController extends AbstractController
{
    /**
     *
     */
    public function index(): Response
    {
		/** @var User $user */
		$user = $this->getUser();

        return $this->render('liste_amis/index.html.twig', [
            'amis' => $user->getAmis(),
        ]);
    }
}
