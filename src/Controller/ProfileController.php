<?php

namespace App\Controller;

use App\Entity\Album;
use App\Entity\Photo;
use App\Entity\Tag;
use App\Entity\User;
use App\Form\FilterType;
use App\Form\PhotoType;
use App\Repository\AlbumRepository;
use App\Repository\PhotoRepository;
use App\Repository\UserRepository;
use Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination;
use Knp\Component\Pager\Paginator;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProfileController extends AbstractController
{
	/**
	 * @param PaginatorInterface $paginator
	 * @param string $pseudo
	 * @param int $page
	 * @param int|null $photo
	 * @return Response
	 */
    public function index(PaginatorInterface $paginator, string $pseudo, int $page=1, int $photo=null)
    {
		$em = $this->getDoctrine()->getManager();

		/****************************/
		/* gestion de l'utilisateur */
        /****************************/

    	/** @var UserRepository $repoUser */
    	$repoUser = $em->getRepository(User::class);

    	/** @var User $profileUser */
    	$profileUser = $repoUser->findOneByPseudo($pseudo);
		/** @var User $user */
		$user = $this->getUser();

		/* if user with pseudo $pseudo does not exist */
		if(is_null($profileUser)) {
			throw $this->createNotFoundException("Pas d'utilisateur avec le pseudo \"" . $pseudo.'"');
		}

		/********************************/
		/* gestion des photos du profil */
        /********************************/

		/** @var PhotoRepository $repoPhoto */
		$repoPhoto = $em->getRepository(Photo::class);
		if($photo!==null){
			$page = $repoPhoto->getPage($repoPhoto->find($photo));
		}

		/* récupère les photos de l'utilisateur */
		$queryBuilder = $repoPhoto->findDisplayableBy(['user'=>$profileUser], ['datePublication' => 'desc']);
        $repoTag = $em->getRepository(Tag::class);
        $tagList =  $repoTag->findTagText($queryBuilder);

		/* création du formulaire de filtres des photos */
        $filterForm = $this->createForm(FilterType::class, null, [
            'attr' => ['id' => 'filter-form'],
            'tagTextList' => $tagList,
            'user' => $profileUser
        ]);

		/* crée le paginateur */
		/** @var SlidingPagination $paginated_photos */
		$paginated_photos = $paginator->paginate($queryBuilder, $page, 9,[
			Paginator::PAGE_OUT_OF_RANGE => Paginator::PAGE_OUT_OF_RANGE_FIX
		]);
		$paginated_photos->setTemplate("/profile/includes/template.pagination.html.twig");

		/************************/
		/* création du contexte */
		/************************/
		$context = [
			'profileUser' => $profileUser,
			'photos' => $paginated_photos,
			'page' => $paginated_photos->getCurrentPageNumber(),
			'displayedPhoto' => $photo,
            'order' => 'DESC',
            'filterForm' => $filterForm->createView()
		];

        /*******************************************/
		/* gestion du formulaire de photo et d'ami */
        /*******************************************/
		if($user === $profileUser){
			$photo = new Photo();

			$form = $this->createForm(PhotoType::class, $photo, [
				'method' => 'POST',
				'attr' => ['id' => 'add_photo_form'],
			]);
			$context['form'] = $form->createView();

			return $this->render('profile/owner.profile.html.twig', $context);
		} else {
			$btnToDisplay = null;
			if($user && $user->getAmis()->contains($profileUser)){
				$relation = $user->getRelationFromAmi($profileUser);
				if($relation->getStatut()){ // statut == true -> demande deja acceptée
					$btnToDisplay = 'remove';
				} else {
					if($relation->getUser1() === $user){ // demande sortante
						$btnToDisplay = 'cancel';
					} else { //demande entrante
						$btnToDisplay = 'answer';
					}
				}
			} else {
				$btnToDisplay = 'add';
			}

            $context['btnAmi'] = $btnToDisplay;

			return $this->render('profile/base.profile.html.twig', $context);
		}
    }
}
