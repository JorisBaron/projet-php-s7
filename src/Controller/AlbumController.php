<?php

namespace App\Controller;

use App\Entity\Album;
use App\Entity\User;
use App\Form\AlbumType;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * fichier AlbumController.php
 *
 * Auteurs pour ce fichier (sauf contre-indications pour une fonction)
 * @author Maurane Glaude
 * @author Tinhinane Kadri
 */
class AlbumController extends AbstractController
{

    /**
     * @param Request $request
     * @return Response
     */
    public function add(Request $request): Response
    {
        $album = new Album();
        $pseudo = $this->getUser()->getPseudo();
        $user = $this->getDoctrine()
                    ->getRepository(User::class)
                    ->findOneBy(["pseudo" => $pseudo]);

        $form = $this->createForm(AlbumType::class, $album, [
            'method' => 'POST',
            'attr' => [
                'id' => 'album_add_form',
                'data-url' => $this->generateUrl('album.add', [
                ]),
            ],
        ]);

        $form->handleRequest($request);

        /* POST */
        if($form->isSubmitted() && $form->isValid()) {
            /* récupérer un entityManager */
            $em = $this->getDoctrine()->getManager();

            $album = $form->getData();
            $user->addAlbum($album);

            /* sauvegarde */
            $em->persist($album);
            $em->persist($user);

            /* lance les requêtes */
            $em->flush();

            $url = $this->generateUrl('album.grid.get', [
                'pseudo' => $pseudo,
                'page' => 1
            ]);
            return new Response($url);
        }

        return $this->render("profile/forms/album.form.html.twig", [
            "form" => $form->createView(),
            "titre_form" => "Ajouter un album"
        ]);
    }

    /**
     * @param PaginatorInterface $paginator
     * @param int $album_id
     * @param int $page
     * @return Response
     */
    public function get_album(PaginatorInterface $paginator, int $album_id, int $page = 1): Response
    {
        $repoAlbum = $this->getDoctrine()
            ->getRepository(Album::class)
            ->findOneBy(["id" => $album_id]);

        $photos = $repoAlbum->getPhotos();

        if(is_null($repoAlbum)) {
            return $this->redirectToRoute("not_found");
        }

        /* gestion de la pagination */
        $repoPhotoPaginated = $paginator->paginate(
            $photos,
            $page,
            9
        );
        $repoPhotoPaginated->setTemplate("/profile/includes/template.pagination.html.twig");
        $repoPhotoPaginated->setUsedRoute('album.get');

        return $this->render("profile/includes/detail.album.html.twig", [
            'photo_list' => $repoPhotoPaginated,
            'album' => $repoAlbum,
        ]);
    }

    /**
     * @param PaginatorInterface $paginator
     * @param string $pseudo
     * @param int $page
     * @param string $order
     * @return Response
     */
    public function get_album_grid(PaginatorInterface $paginator, string $pseudo, int $page, string $order): Response
    {
        $repoUser = $this->getDoctrine()->getRepository(User::class)->findOneByPseudo($pseudo);

        // TODO: code 400
        if (!in_array($order, ['ASC', 'DESC'])) {
            return new Response('', 400);
        }

        // utilisateur saisi non existant
        // TODO: code 404
        if (is_null($repoUser)) {
            return $this->redirectToRoute("not_found", [], 404);
        }

        // créer la requête en fonction de la valeur de $order
        $repoAlbums = $this->getDoctrine()
            ->getRepository(Album::class)
            ->findBy(
                ['user' => $repoUser->getId()],
                ['titre' => $order]
            );

        /* pagination */
        $repoAlbumsPaginated = $paginator->paginate(
            $repoAlbums,
            $page,
            9
        );

        $repoAlbumsPaginated->setTemplate("profile/includes/template.pagination.html.twig");

        return $this->render("profile/includes/grid.album.html.twig", [
            'albums' => $repoAlbumsPaginated,
            'profileUser' => $repoUser,
            'page' => $page,
            'order' => $order
        ]);
    }

    /**
     * @param Request $request
     * @param int $album_id
     * @return Response
     */
    public function edit(Request $request, int $album_id): Response
    {
        /* récupérer l'album */
        $repoAlbum = $this->getDoctrine()->getRepository(Album::class)->findOneBy(['id' => $album_id]);

        $form = $this->createForm(AlbumType::class, $repoAlbum, [
            'method' => 'POST',
            'attr' => [
                'id' => 'album_edit_form',
                'data-url' => $this->generateUrl('album.edit', [
                    'album_id' => $album_id
                ]),
            ],
        ]);

        $form->handleRequest($request);

        /* POST */
        if($form->isSubmitted() && $form->isValid()) {
            /* récupérer un entityManager */
            $em = $this->getDoctrine()->getManager();

            /* sauvegarder les modifications */
            $repoAlbum = $form->getData();
            $em->persist($repoAlbum);
            $em->flush();

            /* retourner sur la page de l'album */
            $url = $this->generateUrl('album.get', [
                'album_id' => $repoAlbum->getId(),
                'page' => 1
            ]);
            return new Response($url);
        }

        return $this->render("profile/forms/album.form.html.twig", [
            "form" => $form->createView(),
            "titre_form" => "Modifier un album"
        ]);
    }

    /**
     * @param int $album_id
     * @return Response
     */
    public function delete(int $album_id): Response
    {
        /* récupérer l'album */
        $repoAlbum = $this->getDoctrine()->getRepository(Album::class)->findOneBy(['id' => $album_id]);

        /* récupérer un entityManager */
        $em = $this->getDoctrine()->getManager();

        /* vérifier si des photos existaient dans l'album et les en sortir */
        foreach($repoAlbum->getPhotos() as $photo) {
            $photo->setAlbum(null);
            $em->persist($photo);
        }

        /* supprimer l'album */
        $em->remove($repoAlbum);

        /* lancer les requêtes */
        $em->flush();

        $url = $this->generateUrl('album.grid.get', [
            'pseudo' => $this->getUser()->getPseudo(),
            'page' => 1
        ]);
        return new Response($url);
    }

}
