<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\EditProfileType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


/**
 * Class UsersController
 * @package App\Controller
 *
 * @author Elhadj Diallo
 */
class UsersController extends AbstractController {
    
    public function index()
    {
      return $this->render('users/index.html.twig');
    }

    public function editProfile(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

		/** @var UserRepository $repoUser */
		$repoUser = $em->getRepository(User::class);

    	
        $newUser = $this->getUser();
        $form = $this->createForm(EditProfileType::class, $newUser);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            	/** @var UploadedFile|null $formPhoto */
			$formPhoto = $form->get('photoFile')->getData();
			if($formPhoto){
				do {
					$photoHash = uniqid("",true).uniqid("",true);
					$photoFilename = $photoHash.'.'.$formPhoto->guessExtension();
				} while($repoUser->findOneByPhoto($photoHash) !== null);

				$formPhoto->move($this->getParameter('pp_directory'), $photoFilename);
				$newUser->setPhoto($photoFilename);
			}
          
            $em->persist($newUser);
            $em->flush();

              $this->addFlash('success', 'Votre profil à été mis à jour avec succès');
             return $this->redirectToRoute('app_users');                  
        }
        return $this->render('users/editprofile.html.twig', [
            'form' => $form->createView(),
        ]);
    }



    public function editPass(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        if($request->isMethod('POST')){
            $em = $this->getDoctrine()->getManager();

            $user = $this->getUser();

            // On vérifie si les 2 mots de passe sont identiques
            if($request->request->get('pass') == $request->request->get('pass2')){
                $user->setPassword($passwordEncoder->encodePassword($user, $request->request->get('pass')));
                $em->flush();
                $this->addFlash('success', 'Votre mot de passe à été mis à jour avec succès');

                return $this->redirectToRoute('app_users');
            }else{
                $this->addFlash('error', 'Les deux mots de passe ne sont pas identiques');
            }
        }

        return $this->render('users/editpass.html.twig');
    }   
    
}