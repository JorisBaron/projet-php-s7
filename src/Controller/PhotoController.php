<?php

namespace App\Controller;

use App\Entity\GenericUser;
use App\Entity\Photo;
use App\Entity\Tag;
use App\Entity\Report;
use App\Entity\ReportedPhoto;
use App\Entity\User;
use App\Form\FilterType;
use App\Form\ChangeAlbumType;
use App\Form\PhotoType;
use App\Repository\PhotoRepository;
use App\Repository\ReportedPhotoRepository;
use App\Repository\UserRepository;
use App\Service\PhotoService;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Query\Expr;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\PropertyAccess\Exception\InvalidArgumentException;
use Symfony\Component\Routing\Annotation\Route;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;


class PhotoController extends AbstractController {
	/**
	 * @param int $photo_id
	 * @return Response
	 */
    public function get_photo(int $photo_id): Response {
		/** @var User $user */
		$user = $this->getUser();

        /* managers */
        $manager = $this->getDoctrine();

		/** @var Photo $photo retrieving asked photo */
        $photo = $manager->getRepository(Photo::class)->find($photo_id);
        $photoUser = $photo->getUser();
        // vérifie le droit d'accès
        if(!($user === $photoUser || $photoUser->getPublic() || $photoUser->isAmi($user))) {
			return new Response(null, 403);
		}

		/* if photo exists */
		if($photo!==null) {
			/* return the photo data */
			return $this->render("profile/includes/detail.photo.html.twig", [
				"photo" => $photo
			]);
		} else {
			return new Response(null, 404);
		}

    }


    /**
     * @param Request $request
     * @param PaginatorInterface $paginator
     * @param string $pseudo
     * @param int $page
     * @param string $order
     * @return Response
     */
    public function get_photos_grid(Request $request, PaginatorInterface $paginator, string $pseudo, int $page, string $order): Response {
		/** @var User $user */
		$user = $this->getUser();

        /* managers */
        $manager = $this->getDoctrine()->getManager();

        /** @var UserRepository $repoUser */
        $repoUser = $manager->getRepository(User::class);
		/** @var User $profileUser retrieving profileUser */
        $profileUser = $repoUser->findOneByPseudo($pseudo);

        if($profileUser === null){
			return new Response("Unknown user", 404);
		}

        // vérifie le droit d'accès
		if(!($user === $profileUser || $profileUser->getPublic() || $profileUser->isAmi($user))) {
			return new Response(
				$this->renderView('profile/includes/not-friend.photo.html.twig'),
				403);
		}

		// vérifie la valeur d'ordonnancement
        if(!in_array($order, ['ASC', 'DESC']))
            return new Response('', 400);

        /** @var PhotoRepository $repoPhoto */
        $repoPhoto = $manager->getRepository(Photo::class);

        $queryBuilder = $repoPhoto->findDisplayableBy(['user'=>$profileUser], ['datePublication' => $order]);

        ///  FILTRES
        $repoTag = $this->getDoctrine()->getRepository(Tag::class);
        $tagList =  $repoTag->findTagText($queryBuilder);

        $form = $this->createForm(FilterType::class, null, [
            'attr' => ['id' => 'filter-form'],
            'tagTextList' => $tagList,
            'user' => $profileUser
        ]);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            $checkedAlbums = $data['albums'];
            $checkedTags = $data['tags'];
            $checkedDateDebut = $data['date_debut'];
            $checkedDateFin = $data['date_fin'];

            $critereFindBy = [
                'user' => $profileUser,
            ];
            if(!$checkedAlbums->isEmpty()) {
                $critereFindBy['album'] = $checkedAlbums;
            }
            $test = $repoPhoto->findDisplayableBy($critereFindBy);

            $testArrayCollection = new ArrayCollection($test);

            // critère perso
            $critereDate = Criteria::create();
            if($checkedDateDebut !== null) {
                $critereDate->andWhere(Criteria::expr()->gte( 'datePublication', new \DateTime($checkedDateDebut)));
            }
            if($checkedDateFin !== null) {
                $critereDate->andWhere(Criteria::expr()->lte( 'datePublication', new \DateTime($checkedDateFin)));
            }

            // application du critère
            $res = $testArrayCollection->matching($critereDate);

            // vérifie si un des tags de chaque photo se trouve dans la liste des tags checked
            $tags = $repoTag->findBy(['text' => $checkedTags]);

            if(!empty($tags)) {
                $tags_id = array_map(function (Tag $t){
                    return $t->getId();
                },$tags);

                $res = array_filter($res->toArray(), function (Photo $p) use ($tags_id){
                    return $p->getTags()->exists(function ($i, Tag $photoTag) use ($tags_id){
                        return in_array($photoTag->getId(), $tags_id);
                    });
                });
            }
        }

        // initialise le paginateur
		$paginated_photos = $paginator->paginate($res, $page, 9);

		// change le rendu de la pagination
        $paginated_photos->setTemplate("/profile/includes/template.pagination.html.twig");

        return $this->render('profile/includes/grid.photo.html.twig', [
            "photos" => $paginated_photos,
            "profileUser" => $profileUser,
            "order" => $order,
            "filterForm" => $form->createView()
        ]);
    }

	/**
	 * @param Request $request
	 * @param PhotoService $photoService
	 * @return Response
	 */
    public function add_photo(Request $request, PhotoService $photoService): Response {
        $em = $this->getDoctrine()->getManager();

        /** @var User $user */
		$user = $this->getUser();
		// vérifie que l'utilisateur est bien connecté
		if($user===null){
			return new Response("Unauthorized", 401);
		}

        $photo = new Photo();
        $photo->setUser($user);

        $form = $this->createForm(PhotoType::class, $photo, [
            'method' => 'POST',
            'attr' => ['id' => 'add_photo_form'],
        ]);


		$form->handleRequest($request);

		// if form valid
		if($form->isSubmitted() && $form->isValid()) {
			// retrieve photo data
			$data = $form->getData();

			// handle the new file
			$photoFile = $form->get('fichierInput')->getData();

			if($photoFile) {
				///** @var PhotoRepository $repoPhoto */
				//$repoPhoto = $em->getRepository(Photo::class);
				//do {
				//	$photoHash = uniqid("",true).uniqid("",true);
				//	$photoFilename = $photoHash.'.'.$photoFile->guessExtension();
				//} while($repoPhoto->findOneByFichier($photoHash) !== null);

				$photoFilename = $photoService->generateHashForPhoto($photoFile->guessExtension());
				$photoFile->move($this->getParameter('photo_directory'), $photoFilename);
				$photo->setFichier($photoFilename);
			}

			// sauvegarde la photo en base
			$em->persist($photo);

			// gère les tags
			foreach($data->getTags() as $tag) {
				$photo->addTag($tag);
				$em->persist($tag);
			}

			// exécute les requêtes
			$em->flush();

			// when data are saved, return a 201 (Created) Response
			return new Response(null, 201);
		}
		else {
			$response = $this->renderView("profile/modals/form.add.photo.html.twig", [
				"form" => $form->createView(),
			]);
			return new Response($response, 400);
		}

    }


	/**
	 * Donne le formulaire d'édition d'une photo prérempli
	 * @param int $photo_id id de la photo pour le pré-remplissage du formulaire
	 * @return RedirectResponse
	 */
    public function getEditForm(int $photo_id): Response {
		/** @var User $user */
		$user = $this->getUser();
		// vérifie que l'utilisateur est bien connecté
		if($user===null){
			return new Response("Unauthorized", 401);
		}

		// récupère l'entity manager
		$em = $this->getDoctrine()->getManager();

		/** @var Photo $photo retrieves photo instance with $photo_id*/
		$photo = $em->getRepository(Photo::class)->find($photo_id);

		// vérifie que l'utilisateur est celui connecté
		if($photo->getUser() !== $user){
			return new Response("Forbidden", 403);
		}

        // formulaire initial
        $form = $this->createForm(PhotoType::class, $photo, [
            'method' => 'PUT',
            'attr' => [
                'id' => 'editPhotoForm',
                'data-photo-id' => $photo->getId()
            ],
        ]);

        // since the file doesn't change, only details, deletes the 'fichier' field
        $form->remove('fichierInput');

        // return the form filled with initial data
		$data = $this->renderView("profile/modals/form.edit.photo.html.twig", [
			"form" => $form->createView(),
		]);
		return new Response($data);

    }

	/**
	 * @param Request $request
	 * @param int $photo_id
	 * @return RedirectResponse
	 */
	public function edit_photo(Request $request, int $photo_id): Response {
		/** @var User $user */
		$user = $this->getUser();
		// vérifie que l'utilisateur est bien connecté
		if($user===null){
			return new Response("Unauthorized", 401);
		}

		// retrieves entity manager
		$em = $this->getDoctrine()->getManager();

		/** @var Photo $photo retrieves photo instance with $photo_id */
		$photo = $em->getRepository(Photo::class)->find($photo_id);
		// vérifie que l'utilisateur est celui connecté
		if($photo->getUser() !== $user){
			return new Response("Forbidden", 403);
		}

		// get the initial data in the photo form
		$form = $this->createForm(PhotoType::class, $photo, [
			'method' => 'PUT',
			'attr' => ['id' => 'editPhotoForm'],
		]);

		// since the file doesn't change, only details, deletes the 'fichier' field
		$form->remove('fichierInput');

		//try {
			// retrieves the submitted form data
			$form->handleRequest($request);
		//} catch(InvalidArgumentException $e){
		//	 les data envoyées n'étaient pas valides
			//$form = $this->createForm(PhotoType::class, $photo, [
			//	'method' => 'PUT',
			//	'attr' => ['id' => 'editPhotoForm'],
			//]);
			//$response = $this->renderView("profile/modals/form.edit.photo.html.twig", [
			//	"form" => $form->createView(),
			//]);
			// add error flash message
			//return new Response($response, 400);
		//}


		// checks if the form is valid
		if($form->isSubmitted() && $form->isValid())
		{
			// retrieve photo data
			$data = $form->getData();

			// add tags in database
			foreach($data->getTags() as $tag)
			{
				$photo->addTag($tag);
				$em->persist($tag);
			}

			// save new photo details into database
			$em->persist($photo);

			// execute queries
			$em->flush();

			// add a success flash message
			//$this->addFlash('info', 'La photo a correctement été mise à jour');

			// render the updated photo with new value
			return new Response(null, 204);
		}
		else {
			// otherwise, render the form with errors
			$response = $this->renderView("profile/modals/form.edit.photo.html.twig", [
				"form" => $form->createView(),
			]);
			// add error flash message
			return new Response($response, 400);
		}
	}

    public function throwAway(int $photo_id): Response {
		try{
			$user = $this->getLoggedUser();
			$photo = $this->photoExistsAndOwned($photo_id, $user);

			$em = $this->getDoctrine()->getManager();
			$photo->throwAway();
			$em->flush();

			return new Response(null, 204);
		} catch(HttpException $ex){
			return $this->generateResponseFromHttpException($ex);
		}
	}

    public function restore(int $photo_id): Response {
		try{
			$user = $this->getLoggedUser();
			$photo = $this->photoExistsAndOwned($photo_id, $user);

			$em = $this->getDoctrine()->getManager();
			$photo->restore();
			$em->flush();

			return new Response(null, 204);
		} catch(HttpException $ex){
			return $this->generateResponseFromHttpException($ex);
		}
	}

    /**
	 * @param int $photo_id
	 * @return Response
	 */
    public function delete_photo(int $photo_id): Response {
    	try{
    		$user = $this->getLoggedUser();
    		$photo = $this->photoExistsAndOwnedOrAdmin($photo_id, $user);

			$em = $this->getDoctrine()->getManager();
			$em->remove($photo);
			$em->flush();

			return new Response(null, 204);
		} catch(HttpException $ex){
    		return $this->generateResponseFromHttpException($ex);
		}
    }

    /**
	 * @param int $photo_id
	 * @return Response
	 *
	 * @author Joris Baron
	 */
    public function reportPhoto(int $photo_id): Response {
		try{
			$user = $this->getUser();
			if(!$user instanceof User){
				throw new AccessDeniedHttpException('Forbidden');
			}
			$photo = $this->photoExists($photo_id);

			$em = $this->getDoctrine()->getManager();

			/** @var ReportedPhotoRepository $repoReport */
			$repoReport = $em->getRepository(ReportedPhoto::class);
			$reportedPhoto = $repoReport->findOneByPhoto($photo);

			if($reportedPhoto === null){
				$reportedPhoto = new ReportedPhoto();
				$reportedPhoto->setPhoto($photo);
				$em->persist($reportedPhoto);
			}

			try {
				$report = new Report();
				$report->setUser($user)
					->setReportedPhoto($reportedPhoto);
				$em->persist($report);
				$em->flush();
			} catch(UniqueConstraintViolationException $e){
				return new Response("alreadyReported", 400);
			}

			return new Response(null, 201);
		} catch(HttpException $ex){
			return $this->generateResponseFromHttpException($ex);
		}
	}

	/**
	 * @param int $photo_id
	 * @return Photo
	 * @throws NotFoundHttpException
	 *
	 * @author Joris Baorn
	 */
    private function photoExists(int $photo_id): Photo {
		$em = $this->getDoctrine()->getManager();

		/** @var Photo $photo */
		$photo = $em->getRepository(Photo::class)->find($photo_id);
		if($photo === null){
			throw $this->createNotFoundException();
		}
		return $photo;
	}

    /**
     * @param Request $request
     * @param int $photo_id
     * @return Response
     */
    public function change_album(Request $request, int $photo_id): Response {
        $repoPhoto = $this->getDoctrine()->getRepository(Photo::class)->findOneBy(['id'=>$photo_id]);
        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm(ChangeAlbumType::class, $repoPhoto, [
            'method' => 'POST',
            'attr' => [
                'id' => 'photo_change_album_form',
                'data-url' => $this->generateUrl('photo.edit.album', [
                    'photo_id' => $photo_id
                ]),
            ]
        ]);

        // On vérifie que le formulaire est soumis
        $form->handleRequest($request);

        // Si il est soumis et valide
        if($form->isSubmitted() && $form->isValid()) {
            // on récupère les informations
            $repoPhoto = $form->getData();

            // et on met à jour l'album de la photo
            $em->persist($repoPhoto);
            $em->flush();

            // on génère l'url pour la suite de la procédure
            $url = $this->generateUrl('photo.get', [
                'photo_id' => $photo_id,
            ]);
            return new Response($url);
        }

        // On retourne le formulaire non traité sinon
        return $this->render('profile/forms/edit.photo.album.html.twig', [
            'form' => $form->createView(),
            'titre_form' => 'Changer la photo d\'album'
        ]);
    }

    /**
	 * @param int $photo_id
	 * @param GenericUser $user
	 * @return Photo
	 * @throws NotFoundHttpException
	 * @throws AccessDeniedHttpException
	 */
    private function photoExistsAndOwned(int $photo_id, GenericUser $user): Photo {
    	$photo = $this->photoExists($photo_id);
		if($photo->getUser() !== $user){
			throw new AccessDeniedHttpException('Forbidden');
		}
		return $photo;
	}

	/**
	 * @param int $photo_id
	 * @param GenericUser $user
	 * @return Photo
	 */
	private function photoExistsAndOwnedOrAdmin(int $photo_id, GenericUser $user): Photo {
		$photo = $this->photoExists($photo_id);
		if($photo->getUser() !== $user && !in_array('ROLE_ADMIN',$user->getRoles())){
			throw new AccessDeniedHttpException('Forbidden');
		}
		return $photo;
	}

	/**
	 * @return User
	 * @throws UnauthorizedHttpException
	 *
	 * @author Joris Baron
	 */
	private function getLoggedUser(): GenericUser {
		/** @var User $user */
		$user = $this->getUser();
		// vérifie que l'utilisateur est bien connecté
		if($user===null){
			throw new UnauthorizedHttpException("Unauthorized");
		}
		return $user;
	}

	private function generateResponseFromHttpException(HttpException $ex, $responseClass = Response::class): Response {
		if(!($responseClass === Response::class || is_subclass_of($responseClass, Response::class))){
			$responseClass = Response::class;
		}
		return new $responseClass(
			$ex->getMessage(),
			$ex->getStatusCode(),
			$ex->getHeaders()
		);
	}
}
