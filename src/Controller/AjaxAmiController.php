<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AjaxAmiController
 * @package App\Controller
 *
 * Controller de gestion d'ami via AJAX
 *
 * @author Joris Baron
 */
class AjaxAmiController extends AbstractController
{
	/**
	 * Supprime un ami ou une demande d'ami en attente
	 * @param string $user
	 * @param string $friend
	 * @return JsonResponse
	 */
    public function delete(string $user, string $friend): JsonResponse {
		$em = $this->getDoctrine()->getManager();

    	/** @var UserRepository $userRepo */
    	$userRepo = $em->getRepository(User::class);

		$u = $userRepo->findOneByPseudo($user);
		$ami = $userRepo->findOneByPseudo($friend);

		if($u && $ami && $u->getAmis()->contains($ami)){
			$u->removeAmi($ami);
			$em->flush();
			return new JsonResponse(true);
		} else {
			return new JsonResponse(false);
		}
    }

	/**
	 * Ajoute une demande d'ami de $user vers $friend
	 * @param string $user
	 * @param string $friend
	 * @return JsonResponse
	 */
	public function add(string $user, string $friend): JsonResponse {
		$em = $this->getDoctrine()->getManager();

		/** @var UserRepository $userRepo */
		$userRepo = $em->getRepository(User::class);

		$u = $userRepo->findOneByPseudo($user);
		$ami = $userRepo->findOneByPseudo($friend);

		if($u && $ami && !$u->getAmis()->contains($ami)){
			$u->demandeAmi($ami);
			$em->flush();
			return new JsonResponse(true);
		} else {
			return new JsonResponse(false);
		}
	}

	/**
	 * Accepte une demande d'ajout en ami de $friend vers $user
	 * @param string $user
	 * @param string $friend
	 * @return JsonResponse
	 */
	public function accept(string $user, string $friend): JsonResponse {
		$em = $this->getDoctrine()->getManager();

		/** @var UserRepository $userRepo */
		$userRepo = $em->getRepository(User::class);

		$u = $userRepo->findOneByPseudo($user);
		$ami = $userRepo->findOneByPseudo($friend);

		if($u && $ami && $u->hasDemandeEnAttente($ami)){
			$u->accepteAmi($ami);
			$em->flush();
			return new JsonResponse(true);
		} else {
			return new JsonResponse(false);
		}
	}
}
