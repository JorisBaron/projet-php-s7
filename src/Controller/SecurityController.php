<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Repository\UserRepository;
use App\Service\PhotoService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
	/**
	 *
	 * @param AuthenticationUtils $authenticationUtils
	 * @return Response
	 */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        // if ($this->getUser()) {
        //     return $this->redirectToRoute('target_path');
        // }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', [
        	'last_username' => $lastUsername,
			'error' => $error
		]);
    }

    /**
     *
     */
    public function logout()
    {
        throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }


    public function inscription(Request $request, UserPasswordEncoderInterface $passwordEncoder, PhotoService $photoService){
		$em = $this->getDoctrine()->getManager();

		/** @var UserRepository $repoUser */
		$repoUser = $em->getRepository(User::class);

    	$newUser = new User();

		$form = $this->createForm(UserType::class, $newUser);
		$form->handleRequest($request);
		if($form->isSubmitted() && $form->isValid()){
			$newUser->setPassword(
				$passwordEncoder->encodePassword(
					$newUser,
					$form->get('plainPassword')->getData()
				)
			);

			/** @var UploadedFile|null $formPhoto */
			$formPhoto = $form->get('photoFile')->getData();
			if($formPhoto){
				$photoFilename = $photoService->generateHashForUser($formPhoto->guessExtension());
				$formPhoto->move($this->getParameter('pp_directory'), $photoFilename);
				$newUser->setPhoto($photoFilename);
			}

			$newUser->addRole('ROLE_USER');

			$em->persist($newUser);
			$em->flush();

			$this->addFlash('success', 'Votre inscription a bien été prise en compte. Vous pouvez désormais vous connecter.');
			return $this->redirectToRoute('app_login');
		}

		return $this->render('security/inscription.html.twig', [
			'form' => $form->createView(),
		]);
	}
}
