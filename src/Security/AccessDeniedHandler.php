<?php


namespace App\Security;


use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Http\Authorization\AccessDeniedHandlerInterface;

class AccessDeniedHandler implements AccessDeniedHandlerInterface {
	private $security;
	private $urlGenerator;
	public function __construct(Security $security, UrlGeneratorInterface $urlGenerator)
	{
		$this->security = $security;
		$this->urlGenerator = $urlGenerator;
	}

	/**
	 * @inheritDoc
	 */
	public function handle(Request $request, AccessDeniedException $accessDeniedException) {
		if($this->security->isGranted('ROLE_ADMIN')){
			return new RedirectResponse($this->urlGenerator->generate('admin'));
		} else {
			return new RedirectResponse($this->urlGenerator->generate('home'));
		}

	}
}