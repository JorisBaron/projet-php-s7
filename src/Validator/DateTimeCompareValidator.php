<?php

namespace App\Validator;

use DateTimeInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Exception\UnexpectedValueException;

class DateTimeCompareValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        /** @var DateTimeCompare $constraint */

		if (!($constraint instanceof DateTimeCompare)) {
			throw new UnexpectedTypeException($constraint, DateTimeCompare::class);
		}

        if (null === $value || '' === $value) {
            return;
        }

		if (! ($value instanceof DateTimeInterface)) {
			// throw this exception if your validator cannot handle the passed type so that it can be marked as invalid
			throw new UnexpectedValueException($value, 'DateTimeInterface');
		}

		$valueTS = $value->getTimestamp();
		$constraintTS = $constraint->datetime->getTimestamp();

		switch($constraint->operator){
			case '==':
			case '=':
			case '===':
				$bool = $valueTS == $constraintTS;
				break;
			case '!=':
			case '!==':
			case '<>':
				$bool = $valueTS != $constraintTS;
				break;
			case '<':
				$bool = $valueTS < $constraintTS;
				break;
			case '<=':
				$bool = $valueTS <= $constraintTS;
				break;
			case '>':
				$bool = $valueTS > $constraintTS;
				break;
			case '>=':
				$bool = $valueTS >= $constraintTS;
				break;
			default:
				$bool = false;
		}

		if(!$bool) {
			// TODO: implement the validation here
			$this->context->buildViolation($constraint->message)
				->setParameter('{{ value }}', $value->format('Y-m-d H:i:s'))
				->addViolation();
		}
    }
}
