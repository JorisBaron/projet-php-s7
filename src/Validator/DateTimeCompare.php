<?php

namespace App\Validator;

use DateTimeInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Exception\InvalidOptionsException;

/**
 * @Annotation
 */
class DateTimeCompare extends Constraint
{
    /*
     * Any public properties become valid options for the annotation.
     * Then, use these in your validator class.
     */
    public $message = "La date n'est pas valide.";

    /** @var DateTimeInterface */
    public $datetime;
    /** @var string */
	public $operator;

    public function __construct($options = null) {
    	parent::__construct($options);

    	$invalidsOptions = [];
		if(!($this->datetime instanceof DateTimeInterface)){
			$invalidsOptions[] = 'datetime';
		}
    	if(!in_array($this->operator, ['==','=','===','!=','!==','<>','<','<=','>','>='])){
    		$invalidsOptions[] = 'operator';
		}

    	if(!empty($invalidsOptions)){
			throw new InvalidOptionsException("Options non valides", $invalidsOptions);
		}
    }

	public function getRequiredOptions() {
		return parent::getRequiredOptions() + ['datetime', 'operator'];
	}
}
